#include <iostream>
#include <limits> //std ignore
#include <vector> //vector
#include <algorithm> // sort
#include <iomanip> // formatting

#include "main.h"


int collect(std::vector<int> &data, int &amount){
    std::cout << "Please enter " << amount << " numbers" << std::endl;
    int sum = 0.0;
    for (int i = 1;i <= amount;++i){
        int input;
        std::cout << i<<": ";
        while (!(std::cin >> input)){
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << "Please only enter Integers" << std::endl
                      << i<<": ";
        }
        sum += input;
        data.push_back(input);
    }
    return sum;
}


int main()
{
    std::vector<int> data;
    int amount;
    double sum, final, median;

    std::cout << "Hello, welcome to the mean calculator, where you put in numbers and we spit out a number!" << std::endl
              << "Please enter the amount of numbers you want to use: ";


    while (!(std::cin >> amount)){
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "Please only enter Integers: ";
    }

    sum = collect(data, amount);

    final = sum / (double)amount;

    std::sort(data.begin(), data.end());
    median = (data[data.size() / 2]);

    std::cout <<std::endl<<std::endl;

    std::cout << std::left << std::setw(15) << std::setfill(' ') << "Total"
              << std::left << std::setw(15) << std::setfill(' ') << "Mean"
              << std::left << std::setw(15) << std::setfill(' ') << "Mean"
              << std::endl;

    std::cout << std::left << std::setw(15) << std::setfill(' ') << sum
              << std::left << std::setw(15) << std::setfill(' ') << final
              << std::left << std::setw(15) << std::setfill(' ') << median
              << std::endl;
    return 0;
}
