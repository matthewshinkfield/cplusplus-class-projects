#include <iostream>
#include "pointofsale.h"


int main()
{
  pointofsale::prepareInventory();
  pointofsale::captureTransaction();
  return 0;
}
