TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    pointofsale.cpp \
    saleitem.cpp

HEADERS += \
    pointofsale.h \
    saleitem.h
