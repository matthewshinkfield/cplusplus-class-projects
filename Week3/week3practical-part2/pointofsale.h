#ifndef POINTOFSALE_H
#define POINTOFSALE_H
#include <string>
#include <iostream>
#include "saleitem.h"

namespace pointofsale {

/**
 * @brief captureTransaction prompts the operator for the items and prints the receipt.
 */
void captureTransaction();

/**
 * @brief printReceipt prints the receipt for a transaction.
 */
void printReceipt();

/**
 * @brief printBusinessDetails print the business details for the top of the receipt.
 */
void printBusinessDetails();

/**
 * @brief displayHeadings prints the column headings.
 */
void printHeadings();

/**
 * @brief printItem print a line item on a receipt
 * @param item the item to print
 * @param quantity the quantity purchased
 */
void printItem(const SaleItem &item, int quantity, bool first = false);

/**
 * @brief calculateTotal calculate the total for the current transaction.
 * @return the total of the current transaction.
 */
double calculateTotal();

/**
 * @brief prepareInventory load the inventory with stock items.
 */
void prepareInventory();


const std::string headings[] = {"Row", "Item", "Qty", "Price", "Subtotal"};
const int totalWidth = 48;
const int colWidths[] = { 4, 19, 4, 8, 9 };

}

#endif // POINTOFSALE_H
