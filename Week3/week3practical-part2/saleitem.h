#ifndef SALEITEM_H
#define SALEITEM_H

#include <string>
/**
 * @brief The SaleItem class represents an item in a transaction or the inventory.
 */
class SaleItem
{
public:
  SaleItem();
  SaleItem(const std::string &code, const std::string &description, double price);
  // Copy constructor
  SaleItem(const SaleItem &other);

  void setPrice(double price);
  void setDescription(const std::string &description);

  double price() const;
  std::string description() const;

private:
  std::string _code;
  std::string _description;
  double _price;
};

#endif // SALEITEM_H
