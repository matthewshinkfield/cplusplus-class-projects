#include "pointofsale.h"
#include <iomanip>
#include <vector>
#include <map>
#include <limits>

namespace pointofsale {

/**
 * @brief The SaleLine struct contains an individual sale item line.
 */
struct SaleLine {
  int quantity;
  SaleItem &item;
};

// Make things a little easier by using several objects and mutators.
using std::cout;
using std::setw;
using std::endl;
using std::left;
using std::right;
using std::cin;
using std::fixed;
using std::setprecision;

// The store's inventory
std::map<std::string, SaleItem> inventory;

// The current transaction (a list of sale items).
std::vector<SaleLine> transaction;

// Print to cout
std::ostream &printer = std::cout;

void captureTransaction()
{
  std::string input;
  while (input != "end") {
    cout << "Please enter an item code (or end to finish): ";
    cin >> input;
    // Search for the item in inventory
    auto it = inventory.find(input);
    if ( it != inventory.end() ) {
      SaleItem &item = it->second;

      std::cout << std::endl
                << std::left << std::setw(8) << std::setfill(' ') << "Price"
                << std::left << std::setw(8) << std::setfill(' ') << "Description" << std::endl;

      std::cout << std::left << std::setw(8) << std::setfill(' ') << item.price()
                << std::left << std::setw(8) << std::setfill(' ') << item.description() << std::endl << std::endl;

      cout << "How many? (0 to cancel) ";
      int qty;
      cin >> qty;
      // Validate qty (must be a positive number)
      while (cin.fail() || qty < 0) {
        cin.clear();
        cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        cout << "Please enter a positive number as a quantity: ";
        cin >> qty;
      }

      // Place item into transaction.
      if (qty > 0){
          transaction.push_back({qty, item});
      }

    } else if (input != "end"){
        cout << "Sorry, this item is not in our inventory" << endl;
    }

  }
  if (transaction.size() > 0) {
    // Display total for transaction.
    cout << "Total Price: $" << setw(6) << fixed << setprecision(2) << calculateTotal() << endl;
    // Ask the operator if a receipt print is needed.
    cout << "Print a receipt? (y/n) ";
    char decision;
    cin >> decision;
    if (tolower(decision) == 'y' || tolower(decision == 'yes')) {
      cout << endl;
      printReceipt();
    }
  } else {
      cout << "Void Transaction -- No Items" << endl;
  }
}

void printReceipt()
{
  printer << std::setfill('-') << setw(totalWidth) << "" << std::setfill(' ') << endl;
  printer << endl << "TAX RECEIPT" << endl;
  printBusinessDetails();
  printer << endl;
  printHeadings();
  double total = 0;
  bool isFirst = true;

  for (const SaleLine &line : transaction){
      SaleItem &item = line.item;
      int quantity = line.quantity;
      printItem(item, quantity, isFirst);
      isFirst = false;
  }
}

void printItem(const SaleItem &item, int quantity, bool first) {
  static int line = 0;
  if (first) { line = 0; }
  printer << setw(colWidths[0]) << left << line
                                << '|' << setw(colWidths[1]) << item.description()
                                << '|' << setw(colWidths[2]) << quantity
                                << '|' << setw(colWidths[3]) << item.price()
                                << '|' << setw(colWidths[4]) << ( item.price() * quantity) << endl;
  line++;
}

void printBusinessDetails()
{
  printer << "ABC Groceries Pty Ltd" << endl;
  printer << "Shop 2, 40 Main Street, Mawson Lakes, SA. ABN is 12 123 456 789" << endl;
}

void printHeadings()
{


  printer << setw(colWidths[0]) << left << headings[0] << '|' << setw(colWidths[1]) << headings[1]
      << '|' << setw(colWidths[2]) << headings[2] << '|' << setw(colWidths[3]) << headings[3]
      << '|' << setw(colWidths[4]) << headings[4] << endl;
}

double calculateTotal()
{
  // Iterate through each line and calculate a total.
  double total = 0;
  for (const SaleLine &line: transaction) {
    total += line.item.price() * line.quantity;
  }
  return total;
}

void prepareInventory()
{
  std::string code = "mars";
  inventory[code] = SaleItem{code, "Mars Bar", 2.5};
  code = "snic";
  inventory[code] = SaleItem{code, "Snickers Bar", 2.5};
  code = "twix";
  inventory[code] = SaleItem{code, "Twix Bar", 2.5};
  code = "coke";
  inventory[code] = SaleItem{code, "CocaCola 600ml", 3.9};
  code = "fant";
  inventory[code] = SaleItem{code, "Fanta 600ml", 3.9};
  code = "spri";
  inventory[code] = SaleItem{code, "Sprite 600ml", 3.9};
  code = "magn";
  inventory[code] = SaleItem{code, "Magnum Classic", 3};
  code = "padd";
  inventory[code] = SaleItem{code, "Paddle Pop", 2};
}

}

