#include "saleitem.h"

SaleItem::SaleItem() : _price{0}
{
}

SaleItem::SaleItem(const std::string &code, const std::string &description, double price)
  : _code{code}, _description{description}, _price{price}
{
}

SaleItem::SaleItem(const SaleItem &other)
  : _code{other._code}, _description{other._description}, _price{other._price}
{
}

void SaleItem::setPrice(double price)
{
  _price = price;
}

void SaleItem::setDescription(const std::string &description)
{
  _description = description;
}

double SaleItem::price() const
{
  return _price;
}

std::string SaleItem::description() const
{
  return _description;
}
