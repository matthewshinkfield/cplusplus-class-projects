#include "vector3d.h"
#include <cmath>

Vector3D::Vector3D(float x, float y, float z)
    : _x(x), _y(y), _z(z) {}

Vector3D::Vector3D(const Vector3D &obj)
    : _x(obj._x), _y(obj._y), _z(obj._z) {}

float Vector3D::distanceTo(const Vector3D &v2) const {
  float x = powf(v2.x() - _x, 2);
  float y = powf(v2.y() - _y, 2);
  float z = powf(v2.z() - _z, 2);
  return std::sqrt(x + y + z);;
}

float Vector3D::x() const { return _x; }
float Vector3D::y() const { return _y; }
float Vector3D::z() const { return _z; }

Vector3D Vector3D::operator+(const Vector3D &vector) {
  float x{_x + vector.x()};
  float y{_y + vector.y()};
  float z{_z + vector.z()};
//  Vector3D vec;
  return Vector3D{x, y, z};
}

bool Vector3D::operator==(const Vector3D &vector) {
    return ((_x==vector.x()) && (_y==vector.y()) && (_z==vector.z()));
//    if (_x == vector.x()) {
//      if (_y == vector.y()) {
//        if (_z == vector.z()) {
//          return true;
//        }
//      }
//    }
//  return false;
}

bool Vector3D::operator!=(const Vector3D &vector) {
  return !(*this == vector);
}
