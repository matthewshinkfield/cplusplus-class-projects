#ifndef VECTOR3D_H
#define VECTOR3D_H

class Vector3D {
public:
  // Constructors

  /**
   * @brief Vector3D - Default Constructor
   * @param const float x
   * @param const float y
   * @param const float z
   */
  Vector3D(float x = 0, float y = 0, float z = 0);

  /**
   * @brief Vector3D - Copy Constructor
   * @param obj
   */
  Vector3D(const Vector3D &obj);

  // Accessors

  /**
   * @brief Coordinate X
   * @return float of x coord
   */
   float x() const;
  /**
   * @brief Coordinate Y
   * @return float of y coord
   */
  float y() const;
  /**
   * @brief Coordinate Z
   * @return float of z coord
   */
  float z() const;

  // Mutators
  /**
   * @brief operator + - Add Vector 1 with Vector 2
   * @param Vector3D vector
   * @return Vector3D - both vectors added
   */
  Vector3D operator+(const Vector3D &vector);

  /**
   * @brief operator = - Set Vector 1 to Vector 2
   * @param vector
   * @return Vector3D - vector overwrite
   */
  operator=(const Vector3D &vector) = delete;

  /**
   * @brief operator == - If two vectors are the same.
   * @param Vector3D vector
   * @return bool
   */
  bool operator==(const Vector3D &vector);

  /**
   * @brief operator == - If two vectors aren't the same.
   * @param Vector3D vector
   * @return bool
   */
  bool operator!=(const Vector3D &vector);

  // Member Functions
  /**
   * @brief distanceTo - distance between Vector 1 and Vector 2
   * @param v2
   * @return float of distance
   */
  float distanceTo(const Vector3D &v2) const;

private:
  // Member Variables
  const float _x;
  const float _y;
  const float _z;
  // private member functions ('helpers');
};
#endif // VECTOR3D_H
