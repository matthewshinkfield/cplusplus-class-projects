#ifndef PROFILEDIALOG_H
#define PROFILEDIALOG_H

#include <QAbstractButton>
#include <QDialog>
#include "profile.h"

namespace Ui {
class ProfileDialog;
}

/**
 * @brief The ProfileDialog class, a popup for viewing editing and displaying Profile's
 * @see Profile
 */
class ProfileDialog : public QDialog {
  Q_OBJECT

 public:
  /**
  * @brief ProfileDialog Constructor
  * @param parent
  */
  explicit ProfileDialog(QWidget *parent = 0);
  ~ProfileDialog();
 public slots:
  /**
 * @brief displayProfile display a profile, and allow either allow it to be editable or not.
 * @param parent
 * @param profile profile to be edited/displayed
 * @param editable
 */
  static void displayProfile(QWidget *parent, Profile *profile, bool editable = true);

 private slots:
  /**
   * @brief setEditable
   * @param editable
   */
  void setEditable(bool editable);

  /**
   * @brief on_imgUploadButton_clicked
   */
  void on_imgUploadButton_clicked();

 private:
  Ui::ProfileDialog *ui;
};

#endif  // PROFILEDIALOG_H
