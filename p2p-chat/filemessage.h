#ifndef FILEMESSAGE_H
#define FILEMESSAGE_H
#include "message.h"

/**
*  @brief The FileMessage class, where a FileMessage is made.
*  @author Matthew Shinkfield
*/
class FileMessage : public Message {
 public:
  /**
 * @brief FileMessage Constructor
 * @param sender
 * @param file
 * @param name
 * @param timestamp
 */
  FileMessage(const QString &sender, const QByteArray &file, const QString &name,
              const QDateTime &timestamp = QDateTime::currentDateTime());
  /**
   * @brief data convert data to the format required by the network.
   * @return a QByteArray containing a network compatible representation of this TextMessage.
   */
  QByteArray data() const override;  // using override here is best practice.
  /**
   * @brief message retrieve the file data
   * @return a QByteArray containing the file data
   */
  const QByteArray &file() const;
  /**
   * @brief name the name of the file
   * @return a QString containg the file name
   */
  const QString &name() const;

 private:
  QByteArray _file;
  QString _name;
};

#endif  // FILEMESSAGE_H
