// Author: Matthew Shinkfield
// Generated: 08 Oct 2017 @ 21:08:57
#include "privatemessage.h"

PrivateMessage::PrivateMessage(const QString &sender, const QString &message,
                               const QString &recipient, const QDateTime &timestamp)
    : Message{sender, timestamp}, _message{message}, _recipient{recipient} {
}
QByteArray PrivateMessage::data() const {
  // The data across the network must start with the type identifier
  QByteArray data = QByteArray::number(Message::PrivateMessage);
  // then a separator
  data += Message::Separator;
  // Then the data required for the message type.
  data += _recipient.toUtf8();  // Use QString::toUtf8() to encode all strings for sending.
  data += Message::Separator;
  data += _message.toUtf8();  // Use QString::toUtf8() to encode all strings for sending.
  return data;
}

const QString &PrivateMessage::message() const {
  return _message;
}

const QString &PrivateMessage::recipient() const {
  return _recipient;
}
