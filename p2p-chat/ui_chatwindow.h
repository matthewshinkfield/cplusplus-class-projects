/********************************************************************************
** Form generated from reading UI file 'chatwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHATWINDOW_H
#define UI_CHATWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ChatWindow
{
public:
    QAction *actionUploadFile;
    QAction *actionUploadImage;
    QAction *actionExit;
    QAction *actionMessageNew;
    QAction *actionMessageInvite;
    QAction *actionMessageKick;
    QAction *actionProfileSearch;
    QAction *actionProfileEdit;
    QAction *actionDownloadImages;
    QAction *actionDownloadFiles;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label;
    QLineEdit *usernameEdit;
    QPushButton *startButton;
    QTabWidget *tabWidget;
    QWidget *mainTab;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_1;
    QListView *listView;
    QListWidget *participantsListWidget;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *lineEdit;
    QPushButton *pushButton;
    QHBoxLayout *horizontalLayout_4;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuUpload;
    QMenu *menuMessage;
    QMenu *menuProfile;
    QMenu *menuDownloads;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *ChatWindow)
    {
        if (ChatWindow->objectName().isEmpty())
            ChatWindow->setObjectName(QStringLiteral("ChatWindow"));
        ChatWindow->resize(571, 389);
        actionUploadFile = new QAction(ChatWindow);
        actionUploadFile->setObjectName(QStringLiteral("actionUploadFile"));
        actionUploadImage = new QAction(ChatWindow);
        actionUploadImage->setObjectName(QStringLiteral("actionUploadImage"));
        actionExit = new QAction(ChatWindow);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        actionMessageNew = new QAction(ChatWindow);
        actionMessageNew->setObjectName(QStringLiteral("actionMessageNew"));
        actionMessageInvite = new QAction(ChatWindow);
        actionMessageInvite->setObjectName(QStringLiteral("actionMessageInvite"));
        actionMessageKick = new QAction(ChatWindow);
        actionMessageKick->setObjectName(QStringLiteral("actionMessageKick"));
        actionProfileSearch = new QAction(ChatWindow);
        actionProfileSearch->setObjectName(QStringLiteral("actionProfileSearch"));
        actionProfileEdit = new QAction(ChatWindow);
        actionProfileEdit->setObjectName(QStringLiteral("actionProfileEdit"));
        actionProfileEdit->setEnabled(true);
        actionDownloadImages = new QAction(ChatWindow);
        actionDownloadImages->setObjectName(QStringLiteral("actionDownloadImages"));
        actionDownloadImages->setEnabled(true);
        actionDownloadFiles = new QAction(ChatWindow);
        actionDownloadFiles->setObjectName(QStringLiteral("actionDownloadFiles"));
        centralWidget = new QWidget(ChatWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_3->addWidget(label);

        usernameEdit = new QLineEdit(centralWidget);
        usernameEdit->setObjectName(QStringLiteral("usernameEdit"));

        horizontalLayout_3->addWidget(usernameEdit);

        startButton = new QPushButton(centralWidget);
        startButton->setObjectName(QStringLiteral("startButton"));
        startButton->setMinimumSize(QSize(100, 0));

        horizontalLayout_3->addWidget(startButton);


        verticalLayout->addLayout(horizontalLayout_3);

        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setTabPosition(QTabWidget::North);
        tabWidget->setTabShape(QTabWidget::Rounded);
        tabWidget->setDocumentMode(true);
        tabWidget->setTabsClosable(true);
        tabWidget->setTabBarAutoHide(true);
        mainTab = new QWidget();
        mainTab->setObjectName(QStringLiteral("mainTab"));
        verticalLayout_2 = new QVBoxLayout(mainTab);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_1 = new QHBoxLayout();
        horizontalLayout_1->setSpacing(6);
        horizontalLayout_1->setObjectName(QStringLiteral("horizontalLayout_1"));
        listView = new QListView(mainTab);
        listView->setObjectName(QStringLiteral("listView"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(listView->sizePolicy().hasHeightForWidth());
        listView->setSizePolicy(sizePolicy);
        listView->setFrameShape(QFrame::WinPanel);
        listView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        listView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        listView->setEditTriggers(QAbstractItemView::NoEditTriggers);
        listView->setDragEnabled(false);
        listView->setDragDropMode(QAbstractItemView::DragDrop);
        listView->setAlternatingRowColors(true);
        listView->setSelectionMode(QAbstractItemView::NoSelection);
        listView->setTextElideMode(Qt::ElideRight);
        listView->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
        listView->setMovement(QListView::Free);
        listView->setSpacing(2);
        listView->setUniformItemSizes(false);
        listView->setBatchSize(10);
        listView->setSelectionRectVisible(true);

        horizontalLayout_1->addWidget(listView);

        participantsListWidget = new QListWidget(mainTab);
        participantsListWidget->setObjectName(QStringLiteral("participantsListWidget"));
        participantsListWidget->setMinimumSize(QSize(140, 0));
        participantsListWidget->setContextMenuPolicy(Qt::CustomContextMenu);
        participantsListWidget->setIconSize(QSize(32, 32));
        participantsListWidget->setSortingEnabled(true);

        horizontalLayout_1->addWidget(participantsListWidget);

        horizontalLayout_1->setStretch(0, 3);
        horizontalLayout_1->setStretch(1, 1);

        verticalLayout_2->addLayout(horizontalLayout_1);

        tabWidget->addTab(mainTab, QString());

        verticalLayout->addWidget(tabWidget);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setEnabled(false);
        lineEdit->setClearButtonEnabled(true);

        horizontalLayout_2->addWidget(lineEdit);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setEnabled(false);

        horizontalLayout_2->addWidget(pushButton);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));

        verticalLayout->addLayout(horizontalLayout_4);

        ChatWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(ChatWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 571, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuUpload = new QMenu(menuFile);
        menuUpload->setObjectName(QStringLiteral("menuUpload"));
        menuUpload->setEnabled(false);
        menuMessage = new QMenu(menuBar);
        menuMessage->setObjectName(QStringLiteral("menuMessage"));
        menuMessage->setEnabled(false);
        menuProfile = new QMenu(menuBar);
        menuProfile->setObjectName(QStringLiteral("menuProfile"));
        menuProfile->setEnabled(false);
        menuDownloads = new QMenu(menuBar);
        menuDownloads->setObjectName(QStringLiteral("menuDownloads"));
        menuDownloads->setEnabled(false);
        ChatWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(ChatWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        ChatWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuMessage->menuAction());
        menuBar->addAction(menuProfile->menuAction());
        menuBar->addAction(menuDownloads->menuAction());
        menuFile->addAction(menuUpload->menuAction());
        menuFile->addSeparator();
        menuFile->addSeparator();
        menuFile->addAction(actionExit);
        menuUpload->addAction(actionUploadFile);
        menuUpload->addAction(actionUploadImage);
        menuMessage->addAction(actionMessageNew);
        menuMessage->addSeparator();
        menuMessage->addAction(actionMessageInvite);
        menuMessage->addAction(actionMessageKick);
        menuProfile->addAction(actionProfileEdit);
        menuProfile->addSeparator();
        menuProfile->addAction(actionProfileSearch);
        menuDownloads->addAction(actionDownloadImages);
        menuDownloads->addAction(actionDownloadFiles);

        retranslateUi(ChatWindow);

        QMetaObject::connectSlotsByName(ChatWindow);
    } // setupUi

    void retranslateUi(QMainWindow *ChatWindow)
    {
        ChatWindow->setWindowTitle(QApplication::translate("ChatWindow", "ChatWindow", Q_NULLPTR));
        actionUploadFile->setText(QApplication::translate("ChatWindow", "File", Q_NULLPTR));
        actionUploadImage->setText(QApplication::translate("ChatWindow", "Image", Q_NULLPTR));
        actionExit->setText(QApplication::translate("ChatWindow", "Exit", Q_NULLPTR));
        actionMessageNew->setText(QApplication::translate("ChatWindow", "New", Q_NULLPTR));
        actionMessageInvite->setText(QApplication::translate("ChatWindow", "Invite", Q_NULLPTR));
        actionMessageKick->setText(QApplication::translate("ChatWindow", "Kick", Q_NULLPTR));
        actionProfileSearch->setText(QApplication::translate("ChatWindow", "Search", Q_NULLPTR));
        actionProfileEdit->setText(QApplication::translate("ChatWindow", "Edit", Q_NULLPTR));
        actionDownloadImages->setText(QApplication::translate("ChatWindow", "Images", Q_NULLPTR));
        actionDownloadFiles->setText(QApplication::translate("ChatWindow", "Files", Q_NULLPTR));
        label->setText(QApplication::translate("ChatWindow", "Nickname:", Q_NULLPTR));
        usernameEdit->setPlaceholderText(QApplication::translate("ChatWindow", "Enter your username", Q_NULLPTR));
        startButton->setText(QApplication::translate("ChatWindow", "Start Chatting!", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(mainTab), QApplication::translate("ChatWindow", "Public Chat", Q_NULLPTR));
        lineEdit->setPlaceholderText(QApplication::translate("ChatWindow", "Enter your message", Q_NULLPTR));
        pushButton->setText(QApplication::translate("ChatWindow", "Send", Q_NULLPTR));
        menuFile->setTitle(QApplication::translate("ChatWindow", "File", Q_NULLPTR));
        menuUpload->setTitle(QApplication::translate("ChatWindow", "Upload", Q_NULLPTR));
        menuMessage->setTitle(QApplication::translate("ChatWindow", "Message", Q_NULLPTR));
        menuProfile->setTitle(QApplication::translate("ChatWindow", "Profile", Q_NULLPTR));
        menuDownloads->setTitle(QApplication::translate("ChatWindow", "Downloads", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ChatWindow: public Ui_ChatWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHATWINDOW_H
