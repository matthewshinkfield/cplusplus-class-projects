/********************************************************************************
** Form generated from reading UI file 'profiledialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROFILEDIALOG_H
#define UI_PROFILEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ProfileDialog
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *nickLabel;
    QLineEdit *nicknameEdit;
    QLabel *locLabel;
    QLineEdit *locationEdit;
    QLabel *timezoneLabel;
    QComboBox *timezoneBox;
    QSpacerItem *verticalSpacer;
    QLabel *imgLabel;
    QSpacerItem *verticalSpacer_2;
    QPushButton *imgUploadButton;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *ProfileDialog)
    {
        if (ProfileDialog->objectName().isEmpty())
            ProfileDialog->setObjectName(QStringLiteral("ProfileDialog"));
        ProfileDialog->resize(240, 271);
        verticalLayout = new QVBoxLayout(ProfileDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        nickLabel = new QLabel(ProfileDialog);
        nickLabel->setObjectName(QStringLiteral("nickLabel"));

        verticalLayout->addWidget(nickLabel);

        nicknameEdit = new QLineEdit(ProfileDialog);
        nicknameEdit->setObjectName(QStringLiteral("nicknameEdit"));
        nicknameEdit->setEnabled(false);

        verticalLayout->addWidget(nicknameEdit);

        locLabel = new QLabel(ProfileDialog);
        locLabel->setObjectName(QStringLiteral("locLabel"));

        verticalLayout->addWidget(locLabel);

        locationEdit = new QLineEdit(ProfileDialog);
        locationEdit->setObjectName(QStringLiteral("locationEdit"));

        verticalLayout->addWidget(locationEdit);

        timezoneLabel = new QLabel(ProfileDialog);
        timezoneLabel->setObjectName(QStringLiteral("timezoneLabel"));

        verticalLayout->addWidget(timezoneLabel);

        timezoneBox = new QComboBox(ProfileDialog);
        timezoneBox->setObjectName(QStringLiteral("timezoneBox"));

        verticalLayout->addWidget(timezoneBox);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        imgLabel = new QLabel(ProfileDialog);
        imgLabel->setObjectName(QStringLiteral("imgLabel"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(imgLabel->sizePolicy().hasHeightForWidth());
        imgLabel->setSizePolicy(sizePolicy);
        imgLabel->setMinimumSize(QSize(0, 0));
        imgLabel->setMaximumSize(QSize(48, 48));
        imgLabel->setScaledContents(true);
        imgLabel->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(imgLabel, 0, Qt::AlignHCenter|Qt::AlignVCenter);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        imgUploadButton = new QPushButton(ProfileDialog);
        imgUploadButton->setObjectName(QStringLiteral("imgUploadButton"));

        verticalLayout->addWidget(imgUploadButton);

        buttonBox = new QDialogButtonBox(ProfileDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Save);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(ProfileDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), ProfileDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ProfileDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(ProfileDialog);
    } // setupUi

    void retranslateUi(QDialog *ProfileDialog)
    {
        ProfileDialog->setWindowTitle(QApplication::translate("ProfileDialog", "Dialog", Q_NULLPTR));
        nickLabel->setText(QApplication::translate("ProfileDialog", "Nickname", Q_NULLPTR));
        locLabel->setText(QApplication::translate("ProfileDialog", "Location", Q_NULLPTR));
        timezoneLabel->setText(QApplication::translate("ProfileDialog", "Timezone", Q_NULLPTR));
        imgLabel->setText(QApplication::translate("ProfileDialog", "Profile Image", Q_NULLPTR));
        imgUploadButton->setText(QApplication::translate("ProfileDialog", "Change Profile Image", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ProfileDialog: public Ui_ProfileDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROFILEDIALOG_H
