// Author: Matthew Shinkfield
// Generated: 08 Oct 2017 @ 21:09:38
#include "identitymessage.h"
#include <QBuffer>

IdentityMessage::IdentityMessage(const QString &sender, const QString &name,
                                 const QString &location, const QString &timezone,
                                 const QByteArray &img, const QDateTime &timestamp)
    : Message(sender, timestamp), _name{name}, _location{location}, _timezone{timezone}, _img{img} {
}

QByteArray IdentityMessage::data() const {
  // The data across the network must start with the type identifier
  QByteArray data = QByteArray::number(Message::IdentityMessage);
  // then a separator
  data += Message::Separator;
  // Then the data required for the message type.
  data += _name.toUtf8();  // Use QString::toUtf8() to encode all strings for sending.
  data += Message::Separator;
  data += _location.toUtf8();  // Use QString::toUtf8() to encode all strings for sending.
  data += Message::Separator;
  data += _timezone.toUtf8();  // Use QString::toUtf8() to encode all strings for sending.
  data += Message::Separator;
  data += _img;  // Use QString::toUtf8() to encode all strings for sending.
  return data;
}

const QString &IdentityMessage::name() const {
  return _name;
}

const QString &IdentityMessage::location() const {
  return _location;
}

const QString &IdentityMessage::timezone() const {
  return _timezone;
}

const QByteArray &IdentityMessage::img() const {
  return _img;
}
