#ifndef IMAGEVIEW_H
#define IMAGEVIEW_H

#include <QDialog>

namespace Ui {
class ImageView;
}
/**
 * @brief The ImageView class
 * @author Matthew Shinkfield
 */
class ImageView : public QDialog {
  Q_OBJECT

 public:
  /**
 * @brief ImageView ItemView Constructor
 * @param parent
 * @param path
 */
  explicit ImageView(QWidget *parent = 0, const QString &path = "");
  ~ImageView();

 private:
  /**
 * @brief displayImage setting up the image or gif to the window
 * @param path
 */
  void displayImage(const QString &path);

  Ui::ImageView *ui;
};

#endif  // IMAGEVIEW_H
