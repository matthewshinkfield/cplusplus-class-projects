#include "chatwindow.h"

#include <QBuffer>
#include <QFileDialog>
#include <QImageReader>
#include <QtWidgets>
#include <memory>

#include "actionmessage.h"
#include "downloaddialog.h"
#include "filemessage.h"
#include "htmldelegate.h"
#include "identitymessage.h"
#include "imagemessage.h"
#include "imageview.h"
#include "messagefactory.h"
#include "privatemessage.h"
#include "profiledialog.h"
#include "textmessage.h"
#include "ui_chatwindow.h"

// TODO: CODE CLEANUP AND CHECK
ChatWindow::ChatWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::ChatWindow) {
  ui->setupUi(this);
  connect(&client, &p2pnetworking::Client::newMessage, this, &ChatWindow::handleMessage);
  connect(&client, &p2pnetworking::Client::newParticipant, this, &ChatWindow::newParticipant);
  connect(&client, &p2pnetworking::Client::participantLeft, this, &ChatWindow::participantLeft);

  // When any chat has a link in it and it's clicked
  connect(ui->listView, &QListView::clicked, this, &ChatWindow::linkClicked);
  // When any chat has a link in it and it's clicked
  connect(ui->participantsListWidget, &QListWidget::clicked, this, &ChatWindow::profileClicked);

  ui->listView->setModel(&historyModel);
  // The HTMLDelegate allows display of HTML formatted text, a subset of HTML is supported.
  // See http://doc.qt.io/qt-5/richtext-html-subset.html - also can be found in built-in help.
  HTMLDelegate *delegate = new HTMLDelegate(this);
  ui->listView->setItemDelegateForColumn(0, delegate);

  ui->tabWidget->tabBar()->tabButton(0, QTabBar::RightSide)->resize(0, 0);

  ui->lineEdit->setEnabled(false);
  ui->pushButton->setEnabled(false);

  QRegExp rx("[^|]*");
  ui->lineEdit->setValidator(new QRegExpValidator(rx, ui->lineEdit));
  ui->usernameEdit->setValidator(new QRegExpValidator(rx, ui->usernameEdit));
}

ChatWindow::~ChatWindow() {
  delete ui;
}

void ChatWindow::handleMessage(QSharedPointer<Message> message) {
  if (QSharedPointer<TextMessage> txt = qSharedPointerDynamicCast<TextMessage>(message)) {
    appendMessage(txt->sender(), txt->message());
  } else if (QSharedPointer<ImageMessage> img = qSharedPointerDynamicCast<ImageMessage>(message)) {
    QString path = (QDir::currentPath() + "/downloads/" + client.nickName() + "/images/");
    QStringList items;
    QString name, folder = "Public Chat";

    if (img->name().indexOf('/') != -1) {
      items = img->name().split('/');
      QWidget *widget = ui->tabWidget->findChild<QWidget *>(items[0]);
      if (widget == nullptr) {
        folder = img->sender();
      } else {
        folder = items[0];
      }
      name = items[1];
    } else {
      name = img->name();
    }
    path += folder + "/";

    QFileInfo fileInfo(path + name);
    if (fileInfo.exists()) {
      int i = 0;
      do {
        i++;
        fileInfo = QFileInfo(path + QString::number(i) + "-" + name);
      } while (fileInfo.exists());
    } else {
      QDir().mkpath(fileInfo.absolutePath());
    }

    QFile file(fileInfo.filePath());
    file.open(QIODevice::WriteOnly);
    file.write(img->img());
    file.close();

    QString displayImage =
        "<a href=\"" + fileInfo.filePath() + "\">" + fileInfo.fileName() + "</a>";
    if (items.size() != 0) {
      QSharedPointer<PrivateMessage> msg(
          new PrivateMessage(img->sender(), displayImage, items[0], img->timestamp()));

      handleMessage(msg);
    } else {
      appendMessage(img->sender(), displayImage);
    }

    if (_images.count(folder) == 0) {
      QStringList list;
      list.append(fileInfo.fileName());
      _images.insert({folder, list});
    } else {
      QStringList list = _images[folder];
      list.append(fileInfo.fileName());
      _images[folder] = list;
    }

  } else if (QSharedPointer<FileMessage> fle = qSharedPointerDynamicCast<FileMessage>(message)) {
    QString path = (QDir::currentPath() + "/downloads/" + client.nickName() + "/files/");

    QStringList items;
    QString name, folder = "Public Chat";

    if (fle->name().indexOf('/') != -1) {
      items = fle->name().split('/');
      QWidget *widget = ui->tabWidget->findChild<QWidget *>(items[0]);
      if (widget == nullptr) {
        folder = fle->sender();
      } else {
        folder = items[0];
      }
      name = items[1];
    } else {
      name = fle->name();
    }

    path += folder + "/";
    QFileInfo fileInfo(path + name);
    if (fileInfo.exists()) {
      int i = 0;
      do {
        i++;
        fileInfo = QFileInfo(path + QString::number(i) + "-" + name);
      } while (fileInfo.exists());
    } else {
      QDir().mkpath(fileInfo.absolutePath());
    }

    QFile file(fileInfo.filePath());

    file.open(QIODevice::WriteOnly);
    file.write(fle->file());
    file.close();

    QString displayFile = "<a href=\"" + fileInfo.filePath() + "\">" + fileInfo.fileName() + "</a>";
    if (items.size() != 0) {
      QSharedPointer<PrivateMessage> msg(
          new PrivateMessage(fle->sender(), displayFile, items[0], fle->timestamp()));

      handleMessage(msg);
    } else {
      appendMessage(fle->sender(), displayFile);
    }

    if (_files.count(folder) == 0) {
      QStringList list;
      list.append(fileInfo.fileName());
      _files.insert({folder, list});
    } else {
      QStringList list = _files[folder];
      list.append(fileInfo.fileName());
      _files[folder] = list;
    }
  } else if (QSharedPointer<ActionMessage> act =
                 qSharedPointerDynamicCast<ActionMessage>(message)) {
    QString name;
    if (client.nickName() == act->operands()[0]) {
      name = act->sender();
    } else {
      name = act->operands()[0];
    }

    if (act->action() == "JOIN") {
      QString to = act->operands()[1];
      QWidget *widget = ui->tabWidget->findChild<QWidget *>(act->operands()[0]);

      QListWidget *users = widget->findChild<QListWidget *>(QString(), Qt::FindChildrenRecursively);
      if (act->operands().size() == 3) {
        QString sentName = act->operands()[2];
        if (_publicName != sentName) {
          _publicName = sentName;
        }
        QStringList operands;
        operands << name;
        operands << act->sender();

        QSharedPointer<ActionMessage> message(
            new ActionMessage(client.nickName(), operands, "JOIN"));
        for (int i = 0; i < users->count(); i++) {
          QListWidgetItem *item = users->item(i);
          client.sendMessage(message, item->text());
        }
        users->addItem(act->sender());

        QListView *chat = widget->findChild<QListView *>(QString(), Qt::FindChildrenRecursively);
        QStringListModel *model = qobject_cast<QStringListModel *>(chat->model());
        QStringList &strlist = _privateChats[name];
        QString newLine = act->sender() + " has joined the chat";
        strlist << newLine;
        model->setStringList(strlist);
      } else {
        QList<QListWidgetItem *> search = users->findItems(to, Qt::MatchExactly);
        if (search.size() == 0) {
          QStringList operands;
          operands << name;
          operands << _publicName;

          QSharedPointer<ActionMessage> message(
              new ActionMessage(client.nickName(), operands, "JOIN"));
          client.sendMessage(message, to);
          users->addItem(to);

          QListView *chat = widget->findChild<QListView *>(QString(), Qt::FindChildrenRecursively);
          QStringListModel *model = qobject_cast<QStringListModel *>(chat->model());
          QStringList &strlist = _privateChats[name];
          QString newLine = to + " has joined the chat";
          strlist << newLine;
          model->setStringList(strlist);
        }
      }
    } else if (act->action() == "LEAVE") {
      QWidget *widget = ui->tabWidget->findChild<QWidget *>(name);

      QListView *chat = widget->findChild<QListView *>(QString(), Qt::FindChildrenRecursively);
      QStringListModel *model = qobject_cast<QStringListModel *>(chat->model());
      QListWidget *users = widget->findChild<QListWidget *>(QString(), Qt::FindChildrenRecursively);
      QStringList &strlist = _privateChats[name];
      QString newLine = act->sender() + " has left the chat";
      strlist << newLine;
      QList<QListWidgetItem *> toRemove = users->findItems(act->sender(), Qt::MatchExactly);
      foreach (QListWidgetItem *item, toRemove) {
        users->removeItemWidget(item);
        delete item;
      }
      model->setStringList(strlist);
      chat->setModel(model);
    } else if (act->action() == "INVITE") {
      if (_publicName != act->operands()[1]) {
        _publicName = act->operands()[1];
      }
      QWidget *widget =
          ui->tabWidget->findChild<QWidget *>(act->operands()[0], Qt::FindChildrenRecursively);
      if (widget != nullptr) {
        return;
      }

      QMessageBox::StandardButton button =
          QMessageBox::question(this, tr("Invite to chat"),
                                "You have been invited by " + act->sender() + " to join the chat " +
                                    name + ".\nDo you wish to join?",
                                QMessageBox::Yes | QMessageBox::No);

      if (button == QMessageBox::Yes) {
        QStringList operands = act->operands();
        operands += act->sender();
        QSharedPointer<ActionMessage> message(
            new ActionMessage(client.nickName(), operands, "JOIN"));
        client.sendMessage(message, act->sender());
        QWidget *widget = ui->tabWidget->widget(createPrivateChat(name));
        QListWidget *users = new QListWidget();
        widget->layout()->addWidget(users);
        ((QHBoxLayout *)widget->layout())->setStretch(1, 1);
        users->addItem(act->sender());
        ui->tabWidget->setCurrentWidget(widget);
        connect(users, &QListWidget::clicked, this, &ChatWindow::profileClicked);
      }
    } else if (act->action() == "KICK") {
      QString kicked = act->operands()[1];
      QWidget *widget = ui->tabWidget->findChild<QWidget *>(name);
      if (_publicName == kicked) {
        QMessageBox::warning(this, tr("Alert"),
                             "You were kicked from " + name + " by " + act->sender());
        delete widget;
      } else {
        QListView *chat = widget->findChild<QListView *>(QString(), Qt::FindChildrenRecursively);
        QStringListModel *model = qobject_cast<QStringListModel *>(chat->model());
        QStringList &strlist = _privateChats[name];
        QString newLine = kicked + " has been kicked by " + act->sender();
        strlist << newLine;
        model->setStringList(strlist);
        QListWidget *users = widget->findChild<QListWidget *>();
        QList<QListWidgetItem *> toRemove = users->findItems(kicked, Qt::MatchExactly);
        foreach (QListWidgetItem *item, toRemove) {
          users->removeItemWidget(item);
          delete item;
        }
      }
    }
  } else if (QSharedPointer<PrivateMessage> pvt =
                 qSharedPointerDynamicCast<PrivateMessage>(message)) {
    QString name = pvt->recipient();

    QWidget *widget = ui->tabWidget->findChild<QWidget *>(name);
    if (widget == nullptr) {
      if (client.nickName() != pvt->sender()) {
        name = pvt->sender();
      }
      widget = ui->tabWidget->findChild<QWidget *>(name);
      if (widget == nullptr) {
        createPrivateChat(name);
      }
    }
    appendMessage(pvt->sender(), pvt->message(), name);
  } else if (QSharedPointer<IdentityMessage> idm =
                 qSharedPointerDynamicCast<IdentityMessage>(message)) {
    Profile *prof = new Profile{idm->sender(), idm->location(), idm->timezone(), idm->img()};
    QString name = prof->name();
    if (_profiles.count(name) != 0) {
      _profiles[name] = QSharedPointer<Profile>(prof);
    } else {
      _profiles.insert({name, QSharedPointer<Profile>(prof)});
    }
    QPixmap pixmap;
    if (pixmap.loadFromData(idm->img(), "PNG")) {
      QList<QListWidgetItem *> toSetIcon =
          ui->participantsListWidget->findItems(name, Qt::MatchExactly);
      QSize size = ui->participantsListWidget->iconSize();
      foreach (QListWidgetItem *item, toSetIcon) {
        item->setIcon(QIcon(pixmap.scaled(size, Qt::KeepAspectRatio, Qt::SmoothTransformation)));
      }
    }
  }
}

void ChatWindow::appendMessage(const QString &from, const QString &message) {
  QString newLine = "<strong style=\"width:150px\">";
  newLine += from;
  newLine += ":</strong>  ";
  newLine += message;

  chatHistory << newLine;
  while (chatHistory.size() > 100) {
    chatHistory.removeFirst();
  }
  historyModel.setStringList(chatHistory);
}

void ChatWindow::appendMessage(const QString &from, const QString &message, const QString &to) {
  QWidget *widget = ui->tabWidget->findChild<QWidget *>(to);
  if (widget == nullptr || _privateChats.count(to) == 0) {
    qDebug() << "Cannot append message, chat may not exist, or may be public, use the other "
                "appendMessage";
    return;
  }

  QListView *chat = widget->findChild<QListView *>(QString(), Qt::FindChildrenRecursively);
  QStringListModel *model = qobject_cast<QStringListModel *>(chat->model());
  QStringList &strlist = _privateChats[to];
  if (message != nullptr) {
    QString newLine = "<strong style=\"width:150px\">";
    newLine += from;
    newLine += ":</strong>  ";
    newLine += message;
    strlist << newLine;
  }
  model->setStringList(strlist);
}

void ChatWindow::newParticipant(const QString &nick) {
  ui->participantsListWidget->addItem(nick);
  client.sendMessage(QSharedPointer<IdentityMessage>(new IdentityMessage(
                         client.nickName(), client.nickName(), _profile->location(),
                         _profile->timezone(), _profile->img())),
                     nick);
}

void ChatWindow::participantLeft(const QString &nick) {
  QList<QWidget *> widgets =
      ui->tabWidget->findChildren<QWidget *>(QString(), Qt::FindDirectChildrenOnly);
  foreach (QWidget *widget, widgets) {
    QList<QListWidget *> users = widget->findChildren<QListWidget *>();
    foreach (QListWidget *user, users) {
      QList<QListWidgetItem *> toRemove = user->findItems(nick, Qt::MatchExactly);
      if (toRemove.count() != 0) {
        QListWidgetItem *item = toRemove[0];
        user->removeItemWidget(item);
        delete item;
      }
    }
  }
}

void ChatWindow::on_startButton_clicked() {
  if (ui->usernameEdit->text() == "") {
    return;
  }
  for (int i = 0; i < ui->participantsListWidget->count(); ++i) {
    QListWidgetItem *item = ui->participantsListWidget->item(i);
    item->text();
  }
  client.setUserName(ui->usernameEdit->text());
  client.start();
  QByteArray imageData;
  {
    // This reads a file from the resources.
    // See Qt documentation on The Qt Resource System.
    QFile imageFile(":/images/User.png");
    if (imageFile.open(QIODevice::ReadOnly)) {
      imageData = imageFile.readAll();
      imageFile.close();
    }
  }
  _profile = QSharedPointer<Profile>(new Profile(client.nickName(), "Unknown", "00", imageData));
  QSharedPointer<IdentityMessage> prof(
      new IdentityMessage(client.nickName(), client.nickName(), "Unknown", "00", imageData));

  client.sendMessage(prof);

  ui->startButton->setEnabled(false);
  ui->usernameEdit->setEnabled(false);

  ui->pushButton->setEnabled(true);
  ui->lineEdit->setEnabled(true);

  ui->menuUpload->setEnabled(true);
  ui->menuMessage->setEnabled(true);
  ui->menuProfile->setEnabled(true);
  ui->menuDownloads->setEnabled(true);

  QString filename = QDir::currentPath() + "/chathistory/" + client.nickName() + "_history.html";
  QDir().mkpath(QFileInfo(filename).absolutePath());
  QFile inputFile(filename);
  if (inputFile.open(QIODevice::ReadOnly)) {
    QTextStream in(&inputFile);
    while (!in.atEnd()) {
      QString line = in.readLine();
      chatHistory << line;
    }
    inputFile.close();
    historyModel.setStringList(chatHistory);
  }
}

void ChatWindow::on_pushButton_clicked() {
  // Create the shared pointer containing a TextMessage.
  if (ui->lineEdit->text() == "") {
    return;
  }

  if (ui->tabWidget->tabText(ui->tabWidget->currentIndex()) != "Public Chat") {
    QString name = ui->tabWidget->tabText(ui->tabWidget->currentIndex());
    QSharedPointer<PrivateMessage> message(
        new PrivateMessage(client.nickName(), ui->lineEdit->text(), name));

    QListWidget *list = ui->tabWidget->currentWidget()->findChild<QListWidget *>(
        QString(), Qt::FindChildrenRecursively);

    if (list == nullptr) {
      client.sendMessage(message, name);
    } else {
      for (int i = 0; i < list->count(); ++i) {
        QListWidgetItem *item = list->item(i);
        client.sendMessage(message, item->text());
      }
    }
    handleMessage(message);
  } else {
    QSharedPointer<TextMessage> message(new TextMessage(client.nickName(), ui->lineEdit->text()));
    // Send to all connected users
    client.sendMessage(message);
    // Display for this computer (messages from this user are not passed to this user from the
    // network) - no point sending data to yourself!
    handleMessage(message);
  }

  ui->lineEdit->clear();
}

void ChatWindow::on_usernameEdit_returnPressed() {
  ui->startButton->click();
}

void ChatWindow::on_lineEdit_returnPressed() {
  ui->pushButton->click();
}

void ChatWindow::on_actionUploadImage_triggered() {
  QString path = QFileDialog::getOpenFileName(this, tr("Open Image"), ".",
                                              tr("Images (*.png *.jpg *.jpeg *.gif)"));
  if (path.isEmpty() || path.isNull()) {
    qDebug() << "Upload image cancelled";
    return;
  }
  QString name;
  QByteArray imageData;
  {
    // This reads a file from the resources.
    // See Qt documentation on The Qt Resource System.
    QFile imageFile(path);
    if (imageFile.open(QIODevice::ReadOnly)) {
      imageData = imageFile.readAll();
      imageFile.close();
    }
    name = QFileInfo(path).fileName();
  }
  int index = ui->tabWidget->currentIndex();
  QString filename;
  if (ui->tabWidget->tabText(index) != "Public Chat") {
    filename = ui->tabWidget->tabText(index) + "/" + name;
  } else {
    filename = name;
  }
  QSharedPointer<ImageMessage> message(new ImageMessage(client.nickName(), imageData, filename));
  if (filename == name) {
    client.sendMessage(message);
  } else {
    QString tabName = ui->tabWidget->tabText(index);
    QList<QListWidgetItem *> findUser =
        ui->participantsListWidget->findItems(tabName, Qt::MatchExactly);
    if (findUser.size() != 0) {
      client.sendMessage(message, tabName);
    } else {
      QWidget *widget = ui->tabWidget->findChild<QWidget *>(tabName, Qt::FindChildrenRecursively);
      QListWidget *users = widget->findChild<QListWidget *>(QString(), Qt::FindChildrenRecursively);
      for (int i = 0; i < users->count(); i++) {
        client.sendMessage(message, users->item(i)->text());
      }
    }
  }
  handleMessage(message);
}

void ChatWindow::on_actionUploadFile_triggered() {
  QString path = QFileDialog::getOpenFileName(this, tr("Open File"), ".");
  if (path.isEmpty() || path.isNull()) {
    qDebug() << "Upload file cancelled";
    return;
  }
  QString name;

  QByteArray imageData;
  {
    QFile imageFile(path);
    if (imageFile.open(QIODevice::ReadOnly)) {
      imageData = imageFile.readAll();
      imageFile.close();
    }
    name = QFileInfo(path).fileName();
  }
  int index = ui->tabWidget->currentIndex();

  QString filename;
  if (ui->tabWidget->tabText(index) != "Public Chat") {
    filename = ui->tabWidget->tabText(index) + "/" + name;
  } else {
    filename = name;
  }
  QSharedPointer<FileMessage> message(new FileMessage(client.nickName(), imageData, filename));
  if (filename == name) {
    client.sendMessage(message);
  } else {
    QString tabName = ui->tabWidget->tabText(index);
    QList<QListWidgetItem *> findUser =
        ui->participantsListWidget->findItems(tabName, Qt::MatchExactly);
    if (findUser.size() != 0) {
      client.sendMessage(message, tabName);
    } else {
      QWidget *widget = ui->tabWidget->findChild<QWidget *>(tabName, Qt::FindChildrenRecursively);
      QListWidget *users = widget->findChild<QListWidget *>(QString(), Qt::FindChildrenRecursively);
      for (int i = 0; i < users->count(); i++) {
        client.sendMessage(message, users->item(i)->text());
      }
    }
  }
  handleMessage(message);
}

void ChatWindow::on_actionExit_triggered() {
  close();
}

void ChatWindow::on_actionMessageNew_triggered() {
  QStringList list;
  for (int i = 0; i < ui->participantsListWidget->count(); i++) {
    list.append(ui->participantsListWidget->item(i)->text());
  }
  QInputDialog *input = new QInputDialog();
  input->setComboBoxEditable(false);
  input->setComboBoxItems(list);
  bool ok = true;
  QString response = QInputDialog::getItem(this, "Private Message",
                                           "Select user or enter room name", list, 0, true, &ok);

  if (ok == false || response == "")
    return;

  ui->tabWidget->setCurrentIndex(createPrivateChat(response));
  return;
}

void ChatWindow::on_actionMessageInvite_triggered() {
  int index = ui->tabWidget->currentIndex();
  QString name = ui->tabWidget->tabText(index);

  // If name of tab is public chat
  if (name == "Public Chat") {
    QMessageBox::warning(this, tr("Alert"),
                         tr("This is the Public Chat, cannot preform this action"));
    return;
  }

  QStringList list;
  for (int i = 0; i < ui->participantsListWidget->count(); i++) {
    QString name = ui->participantsListWidget->item(i)->text();
    list.append(name);
  }

  // If name of tab is a users name
  if (list.contains(name)) {
    QMessageBox::warning(this, tr("Alert"),
                         tr("This is a Private Chat, cannot preform this action"));
    return;
  }

  QWidget *widget = ui->tabWidget->currentWidget();
  QListWidget *users = widget->findChild<QListWidget *>(QString(), Qt::FindChildrenRecursively);
  if (users != nullptr) {
    // Remove all users from a user list if in a group chat.
    for (int i = 0; i < users->count(); i++) {
      QString name = users->item(i)->text();
      if (list.contains(name)) {
        list.removeAll(name);
      }
    }
  }

  // If list is empty.
  if (list.isEmpty()) {
    QMessageBox::warning(
        this, tr("Alert"),
        tr("There is no one else to add to this chat, cannot preform this action"));
    return;
  }

  QInputDialog *input = new QInputDialog();
  input->setComboBoxItems(list);
  bool ok = true;
  QString response =
      QInputDialog::getItem(this, "Invite User", "Select user to invite", list, 0, false, &ok);

  if (!ok) {
    return;
  }

  // If users QListWidget doesn't exist, create it.
  if (users == nullptr) {
    users = new QListWidget();
    widget->layout()->addWidget(users);
    ((QHBoxLayout *)widget->layout())->setStretch(1, 1);
    connect(users, &QListWidget::clicked, this, &ChatWindow::profileClicked);
  }

  QStringList operands;
  operands << name;
  operands << response;
  QSharedPointer<ActionMessage> message(new ActionMessage(client.nickName(), operands, "INVITE"));
  client.sendMessage(message, response);
}

void ChatWindow::on_actionMessageKick_triggered() {
  int index = ui->tabWidget->currentIndex();
  QString name = ui->tabWidget->tabText(index);

  // If name of tab is public chat
  if (name == "Public Chat") {
    QMessageBox::warning(this, tr("Alert"),
                         tr("This is the Public Chat, cannot preform this action"));
    return;
  }

  QStringList list;
  for (int i = 0; i < ui->participantsListWidget->count(); i++) {
    QString name = ui->participantsListWidget->item(i)->text();
    list.append(name);
  }

  // If name of tab is a users name
  if (list.contains(name)) {
    QMessageBox::warning(this, tr("Alert"),
                         tr("This is a Private Chat, cannot preform this action"));
    return;
  }

  QWidget *widget = ui->tabWidget->currentWidget();
  QListWidget *users = widget->findChild<QListWidget *>(QString(), Qt::FindChildrenRecursively);

  if (users == nullptr || users->count() == 0) {
    QMessageBox::warning(this, tr("Alert"),
                         tr("There are no users to kick, cannot preform this action"));
    return;
  }
  list.clear();
  for (int i = 0; i < users->count(); i++) {
    QString name = users->item(i)->text();
    list.append(name);
  }

  QInputDialog *input = new QInputDialog();
  input->setComboBoxItems(list);
  bool ok = true;
  QString response =
      QInputDialog::getItem(this, "Kick User", "Select user to kick", list, 0, false, &ok);

  if (!ok) {
    return;
  }

  QStringList operands;
  operands << name;
  operands << response;
  QSharedPointer<ActionMessage> message(new ActionMessage(client.nickName(), operands, "KICK"));
  foreach (QString name, list) { client.sendMessage(message, name); }
  handleMessage(message);
}

void ChatWindow::on_actionProfileEdit_triggered() {
  ProfileDialog::displayProfile(this, _profile.data());
  QSharedPointer<IdentityMessage> identity(
      new IdentityMessage(client.nickName(), client.nickName(), _profile->location(),
                          _profile->timezone(), _profile->img()));
  client.sendMessage(identity);
}

void ChatWindow::on_actionProfileSearch_triggered() {
  QStringList list;
  for (int i = 0; i < ui->participantsListWidget->count(); i++) {
    list.append(ui->participantsListWidget->item(i)->text());
  }
  QInputDialog *input = new QInputDialog();
  input->setComboBoxItems(list);
  bool ok = true;
  QString response = QInputDialog::getItem(
      this, "Profile Search", "Select user to view their profile", list, 0, false, &ok);

  if (ok == false || response == "")
    return;

  if (_profiles.count(response) != 0) {
    ProfileDialog::displayProfile(this, _profiles.at(response).data(), false);
  }
}

void ChatWindow::on_actionDownloadImages_triggered() {
  QString path = (QDir::currentPath() + "/downloads/" + client.nickName() + "/images/");
  if (DownloadDialog::displayDownloads(this, _images, path)) {
    QFileInfo fileInfo(path);
    QString fileType = fileInfo.suffix();
    QString savePath = QFileDialog::getSaveFileName(this, tr("Save Image"), fileInfo.filePath(),
                                                    fileType.toUpper() + " (*." + fileType + ")");
    if (savePath.isEmpty() || savePath.isNull()) {
      qDebug() << "Upload image cancelled";
      return;
    }
    QFile file(fileInfo.filePath());
    file.copy(path, savePath);
  }
}

void ChatWindow::on_actionDownloadFiles_triggered() {
  QString path = (QDir::currentPath() + "/downloads/" + client.nickName() + "/files/");
  if (DownloadDialog::displayDownloads(this, _files, path)) {
    QFileInfo fileInfo(path);
    QString fileType = fileInfo.suffix();
    QString savePath = QFileDialog::getSaveFileName(this, tr("Save File"), fileInfo.filePath(),
                                                    fileType.toUpper() + " (*." + fileType + ")");
    if (savePath.isEmpty() || savePath.isNull()) {
      qDebug() << "Upload image cancelled";
      return;
    }
    QFile file(fileInfo.filePath());
    file.copy(path, savePath);
  }
}

void ChatWindow::linkClicked(const QModelIndex &index) {
  QString html = index.data().toString();
  int startlink = html.indexOf("<a href=");
  if (startlink == -1) {
    return;
  }
  int endlink = html.indexOf(">", startlink + 1);
  QString path = html.mid(startlink + 9, endlink - startlink - 10);
  if (QImageReader(path).canRead()) {
    ImageView *imgview = new ImageView(this, path);
    imgview->show();
  } else {
    QUrl url = QUrl::fromUserInput(path);
    QTextDocument doc;
    doc.setHtml(html);
    if (!QDesktopServices::openUrl(url)) {
      ui->statusBar->showMessage("Unable to open file, it may not exist", 5000);
    }
  }
}

void ChatWindow::profileClicked(const QModelIndex &index) {
  QString clicked = index.data().toString();
  if (_profiles.count(clicked) != 0) {
    ProfileDialog::displayProfile(this, _profiles.at(clicked).data(), false);
  }
}

void ChatWindow::on_tabWidget_tabCloseRequested(int index) {
  QString name = ui->tabWidget->widget(index)->objectName();
  for (int i = 0; i < ui->participantsListWidget->count(); i++) {
    if (ui->participantsListWidget->item(i)->text() == name) {
      delete ui->tabWidget->widget(index);

      return;
    }
  }
  QSharedPointer<ActionMessage> message(
      new ActionMessage(client.nickName(), QStringList() << name, "LEAVE"));
  client.sendMessage(message);
  handleMessage(message);
  delete ui->tabWidget->widget(index);
}

void ChatWindow::closeEvent(QCloseEvent *event) {
  QDir dir(QDir::currentPath() + "/downloads/" + client.nickName());
  if (dir.exists()) {
    if (!dir.removeRecursively()) {
      QMessageBox::question(this, "Error Closing",
                            tr("Please close all files opened by this program"), QMessageBox::Ok);
      event->ignore();
      return;
    }
  }
  QString filename = QDir::currentPath() + "/chathistory/" + client.nickName() + "_history.html";
  QDir().mkpath(QFileInfo(filename).absolutePath());
  QFile fOut(filename);
  if (fOut.open(QFile::WriteOnly | QFile::Text)) {
    QTextStream s(&fOut);
    for (QString str : chatHistory) {
      if (str.contains("<a href") && !str.contains("<strike>")) {
        QString sender = str.left(str.indexOf("</strong>") + 9);
        str.remove(sender);
        QTextDocument doc;
        doc.setHtml(str);
        str = doc.toPlainText();
        str = str.prepend("<s>");
        str = str.append("</s>");
        str = str.prepend(sender + "  ");
      }
      s << str.toUtf8() + "\n";
    }
    s << "<strong> END OF CHAT @ " +
            QDateTime::currentDateTime().toString("hh:mma 'on' dddd, dd MMMM yyyy") + "</strong>\n";
    s << "-------------------------------------------------------------------------------";
    fOut.close();
  }
}

int ChatWindow::createPrivateChat(const QString &name) {
  QWidget *widget = ui->tabWidget->findChild<QWidget *>(name);
  if (widget != nullptr) {
    return ui->tabWidget->indexOf(widget);
  }
  // Holder Widget
  widget = new QWidget();
  widget->setObjectName(name);
  // Layout Type with Widget as parent
  QHBoxLayout *layout = new QHBoxLayout(widget);

  // Left Widget, stretch scale: 2
  QListView *chat = new QListView();

  // Model for Left Widget
  HTMLDelegate *delegate = new HTMLDelegate(this);
  QStringListModel *privateModel = new QStringListModel(chat->model());

  // Properties for Left Widget
  chat->setItemDelegate(delegate);
  chat->setSpacing(2);
  chat->setEditTriggers(QAbstractItemView::NoEditTriggers);
  chat->setAlternatingRowColors(true);
  chat->setTextElideMode(Qt::TextElideMode::ElideNone);
  chat->setVerticalScrollMode(QAbstractItemView::ScrollMode::ScrollPerPixel);

  // Chat for Left Widget
  QStringList strlist;
  if (_privateChats.count(name) != 0) {
    strlist = _privateChats[name];
  } else {
    _privateChats.insert({name, strlist});
  }
  QString newLine = "Private chat started with " + name;
  strlist << newLine;
  // Insert chat to a Map: QString, QStringList
  _privateChats[name] = strlist;
  privateModel->setStringList(strlist);

  // Set Model to edited
  chat->setModel(privateModel);

  // Add Widgets and set scale
  layout->addWidget(chat);
  layout->setStretch(0, 2);
  connect(chat, &QListView::clicked, this, &ChatWindow::linkClicked);
  return ui->tabWidget->addTab(widget, name);
}
