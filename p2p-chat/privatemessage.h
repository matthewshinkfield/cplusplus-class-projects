#ifndef PRIVATEMESSAGE_H
#define PRIVATEMESSAGE_H
#include "message.h"

/**
*  @brief The PrivateMessage Class, where a PrivateMessage is made.
*  @author Matthew Shinkfield
*/
class PrivateMessage : public Message {
 public:
  PrivateMessage(const QString &sender, const QString &message, const QString &recipient,
                 const QDateTime &timestamp = QDateTime::currentDateTime());
  /**
   * @brief data convert data to the format required by the network.
   * @return a QByteArray containing a network compatible representation of this TextMessage.
   */
  QByteArray data() const override;  // using override here is best practice.

  /**
   * @brief message retrieve the message text
   * @return a QString containing the message content
   */
  const QString &message() const;

  /**
   * @brief recipient recipint of the message
   * @return a QString containing the recipient name
   */
  const QString &recipient() const;

 private:
  QString _message;
  QString _recipient;
};

#endif  // PRIVATEMESSAGE_H
