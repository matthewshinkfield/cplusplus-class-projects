// Author: Matthew Shinkfield
// Generated: 08 Oct 2017 @ 21:09:27
#include "actionmessage.h"

ActionMessage::ActionMessage(const QString &sender, const QStringList &operands,
                             const QString &action, const QDateTime &timestamp)
    : Message(sender, timestamp), _operands{operands}, _action{action} {
}

QByteArray ActionMessage::data() const {
  // The data across the network must start with the type identifier
  QByteArray data = QByteArray::number(Message::ActionMessage);
  // then a separator
  data += Message::Separator;
  data += _action;
  data += Message::Separator;
  // Then the data required for the message type.
  data += _operands.join("|");
  //  data += _operand.toUtf8();  // Use QString::toUtf8() to encode all strings for sending.
  return data;
}

const QStringList &ActionMessage::operands() const {
  return _operands;
}

const QString &ActionMessage::action() const {
  return _action;
}
