#include "imageview.h"
#include <QDebug>
#include <QDesktopWidget>
#include <QFileInfo>
#include <QImageReader>
#include <QMessageBox>
#include <QMovie>
#include <QResizeEvent>
#include <QStyle>
#include "ui_imageview.h"

ImageView::ImageView(QWidget *parent, const QString &path)
    : QDialog(parent), ui(new Ui::ImageView) {
  ui->setupUi(this);
  setModal(false);
  setAttribute(Qt::WA_DeleteOnClose);
  displayImage(path);
}

ImageView::~ImageView() {
  delete ui;
}

void ImageView::displayImage(const QString &path) {
  QImageReader reader(path);
  if (!reader.canRead()) {
    QMessageBox::warning(this, tr("Alert"), tr("Image could not be opened :("));
    close();
    return;
  }
  int w = width();
  int h = height();
  if (reader.format() == "gif") {
    QMovie *movie = new QMovie(path, "GIF", this);
    QSize size = movie->scaledSize();
    if (size.width() < w) {
      w = size.width();
    }
    if (size.height() < h) {
      h = size.height();
    }
    ui->imageLabel->setMovie(movie);
    movie->start();
    setFixedSize(movie->scaledSize());
  } else {
    QPixmap pixmap = QPixmap::fromImageReader(&reader);

    if (pixmap.width() < w) {
      w = pixmap.width();
    }
    if (pixmap.height() < h) {
      h = pixmap.height();
    }
    pixmap = pixmap.scaled(w, h, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, pixmap.size(),
                                    qApp->desktop()->availableGeometry()));
    setFixedSize(pixmap.size());
    ui->imageLabel->setPixmap(pixmap.scaled(size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
  }

  QFileInfo fileInfo(path);
  setWindowTitle(fileInfo.fileName());
}
