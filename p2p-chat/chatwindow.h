#ifndef CHATWINDOW_H
#define CHATWINDOW_H

#include <QMainWindow>
#include <QStringList>
#include <QStringListModel>
#include <map>
#include "./Networking/client.h"
#include "message.h"
#include "privatemessage.h"
#include "profile.h"

namespace Ui {
class ChatWindow;
}

/**
 * @brief The ChatWindow class The main window, where everything happens, besides somethings.
 * @author Matthew Shinkfield
 */
class ChatWindow : public QMainWindow {
  Q_OBJECT

 public:
  /**
 * @brief ChatWindow Constructor
 * @param parent
 */
  explicit ChatWindow(QWidget *parent = 0);
  ~ChatWindow();

 public slots:
  /**
   * @brief handleMessage Handles the input of all Message types and client processes it
   * @param message type of Message
   */
  void handleMessage(QSharedPointer<Message> message);
  /**
   * @brief appendMessage Append a message to the Public Chat
   * @param from user it is sent from
   * @param message
   */
  void appendMessage(const QString &from, const QString &message);
  /**
   * @brief appendMessage Apppend a message to a specific chat
   * @param from user it is sent from
   * @param message
   * @param to chat it is sent to
   */
  void appendMessage(const QString &from, const QString &message, const QString &to);

 private slots:
  /**
   * @brief newParticipant When a new participant enters the public chat room
   * @param nick
   */
  void newParticipant(const QString &nick);

  /**
   * @brief participantLeft When a participant leaves the public chat room
   * @param nick
   */
  void participantLeft(const QString &nick);

  // Buttons
  /**
   * @brief on_startButton_clicked Start Button
   */
  void on_startButton_clicked();

  /**
   * @brief on_pushButton_clicked Send Message Button
   */
  void on_pushButton_clicked();

  // Trigger Buttons
  /**
     * @brief on_usernameEdit_returnPressed When return key is pressed in usernameEdit text box
     */
  void on_usernameEdit_returnPressed();
  /**
   * @brief on_lineEdit_returnPressed When return key is pressed in the lineEdit/message text box,
   * triggered pushButton::clicked;
   */
  void on_lineEdit_returnPressed();

  // Menu Bar Actions
  /**
   * @brief on_actionUploadFile_triggered when File > Upload > Image is triggered, user is prompted
   * with an upload box
   */
  void on_actionUploadFile_triggered();

  /**
   * @brief on_actionUploadImage_triggered when File > Upload > Image is triggered, user is prompted
   * with an upload box
   */
  void on_actionUploadImage_triggered();

  /**
   * @brief on_actionExit_triggered when File > Exit is triggered, user is exited from application,
   * unless program cannot delete sent/recieved files.
   */
  void on_actionExit_triggered();

  /**
   * @brief on_actionMessageNew_triggered when Message > New is triggered, user is prompted a box to
   * select a username or enter a new chat name, unless chat already exists it then switches to that
   * chat
   */
  void on_actionMessageNew_triggered();
  /**
   * @brief on_actionMessageInvite_triggered when Message > Invite is triggered, user is prompted a
   * box to select a username to invite to a chat, unless the chat is not a group chat.
   */
  void on_actionMessageInvite_triggered();
  /**
   * @brief on_actionMessageKick_triggered when Message > Kick is triggered, user is prompted a box
   * to select a user to kick from a chat, unless the chat is not a group chat.
   */
  void on_actionMessageKick_triggered();

  /**
   * @brief on_actionProfileEdit_triggered when Profile > Edit is triggered, user is prompted a
   * dialog to view/edit their profile
   */
  void on_actionProfileEdit_triggered();
  /**
   * @brief on_actionProfileSearch_triggered when Profile > Search is triggered, user is prompted a
   * box to select a user, then a dialog box is shown with their profile
   */
  void on_actionProfileSearch_triggered();

  /**
   * @brief on_actionDownloadImages_triggered when Downloads > Images is triggered, user is prompted
   * a dialog with a list of downloaded images for them to select and save
   */
  void on_actionDownloadImages_triggered();
  /**
   * @brief on_actionDownloadFiles_triggered when Downloads > Files is triggered, user is prompted a
   * dialog with a list of downloaded files for them to select and save
   */
  void on_actionDownloadFiles_triggered();

  // Click Events
  /**
   * @brief linkClicked When chat is clicked and is a link, user opens that link
   * @param index of item/text clicked
   */
  void linkClicked(const QModelIndex &index);

  /**
   * @brief profileClicked When user clicks a profile on the right of any chat, a dialog is shown
   * with that profile
   * @param index of item/profile
   */
  void profileClicked(const QModelIndex &index);

  // Tab Close Event
  /**
   * @brief on_tabWidget_tabCloseRequested When user closes a tab, they are exited from their group
   * chat/private chat
   * @param index
   */
  void on_tabWidget_tabCloseRequested(int index);

 private slots:
  /**
   * @brief closeEvent When program is closed
   * @param event
   */
  void closeEvent(QCloseEvent *event);

 private:
  /**
   * @brief createPrivateChat Creates a new tab for a private chat of a specific name. Can be a
   * username a group chat name.
   * @param name of chat
   * @return the tab index added
   */
  int createPrivateChat(const QString &name);

  Ui::ChatWindow *ui;
  p2pnetworking::Client client;
  QStringList chatHistory;
  QStringListModel historyModel;

  // List of chats
  std::map<QString, QStringList> _privateChats;

  // User Profile
  QSharedPointer<Profile> _profile;

  // List of other profiles
  std::map<QString, QSharedPointer<Profile>> _profiles;
  // List if sent images
  std::map<QString, QStringList> _images;
  // List of sent files
  std::map<QString, QStringList> _files;
  QString _publicName;
};

#endif  // CHATWINDOW_H
