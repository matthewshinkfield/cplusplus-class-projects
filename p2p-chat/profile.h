#ifndef PROFILE_H
#define PROFILE_H

#include <QFile>
#include <QString>
#include <QTimeZone>
/**
*  @brief a Profile for local storage of other users based off IdentityMessage.
*  @author Matthew Shinkfield
*  @see IdentityMessage
*/
class Profile {
 public:
  /**
 * @brief Profile Constructor
 * @param name
 * @param location
 * @param timezone
 * @param img
 */
  Profile(const QString &name, const QString &location, const QString &timezone,
          const QByteArray &img);

  /**
   * @brief name retrieve the name of the user
   * @return a QString with the users name
   */
  const QString &name() const;
  /**
   * @brief location retrieve location of the user
   * @return a QString with the users location
   */
  const QString &location() const;
  /**
   * @brief timezone retrieve the timezone of the user
   * @return a QString with the users location
   */
  const QString &timezone() const;
  /**
   * @brief img retrieve the image data of the user
   * @return a QByteArray containing the image data of the users image
   */
  const QByteArray &img() const;

  /**
   * @brief setLocation set the location of the user
   * @param location
   */
  void setLocation(const QString &location);
  /**
   * @brief setTimezone set the timezone of the user
   * @param timezone
   */
  void setTimezone(const QString &timezone);
  /**
   * @brief setImg set the image of the user
   * @param img
   */
  void setImg(const QByteArray &img);

 private:
  QString _name;
  QString _location;
  QString _timezone;
  QByteArray _img;
};

#endif  // PROFILE_H
