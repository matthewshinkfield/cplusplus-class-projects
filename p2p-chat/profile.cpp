// Author: Matthew Shinkfield
// Generated: 04 Nov 2017 @ 18:35:02
#include "profile.h"

Profile::Profile(const QString &name, const QString &location, const QString &timezone,
                 const QByteArray &img)
    : _name{name}, _location{location}, _timezone{timezone}, _img{img} {
}

const QString &Profile::name() const {
  return _name;
}

const QString &Profile::location() const {
  return _location;
}

const QString &Profile::timezone() const {
  return _timezone;
}

const QByteArray &Profile::img() const {
  return _img;
}

void Profile::setLocation(const QString &location) {
  _location = location;
}

void Profile::setTimezone(const QString &timezone) {
  _timezone = timezone;
}

void Profile::setImg(const QByteArray &img) {
  _img = img;
}
