#include "profiledialog.h"
#include "ui_profiledialog.h"

#include <QBuffer>
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <QPixmap>
#include <QTimeZone>

ProfileDialog::ProfileDialog(QWidget *parent) : QDialog(parent), ui(new Ui::ProfileDialog) {
  ui->setupUi(this);
  QStringList timezones;

  // clang-format off
  timezones << "-1200" << "-1100" << "-1000" << "-930"  << "-900"  << "-800"  << "-700" << "-600" << "-500" << "-400" << "-330"
            << "-300"  << "-200"  << "-100"  << "00"    << "+100"  << "+200"  << "+300" << "+330" << "+400" << "+430" << "+500"
            << "+530"  << "+545"  << "+600"  << "+630"  << "+700"  << "+800"  << "+830" << "+845" << "+900" << "+930" << "+1000"
            << "+1030" << "+1100" << "+1200" << "+1245" << "+1300" << "+1400";
  // clang-format on

  ui->timezoneBox->addItems(timezones);
}

ProfileDialog::~ProfileDialog() {
  delete ui;
}

void ProfileDialog::displayProfile(QWidget *parent, Profile *profile, bool editable) {
  ProfileDialog dialog(parent);
  dialog.ui->nicknameEdit->setText(profile->name());
  dialog.ui->locationEdit->setText(profile->location());
  dialog.ui->timezoneBox->setCurrentText(profile->timezone());

  QPixmap img;
  if (!img.loadFromData(profile->img(), "PNG")) {
    img = (QPixmap(":/images/User.png"));
  }
  img = img.scaled(48, 48, Qt::KeepAspectRatio, Qt::SmoothTransformation);
  dialog.ui->imgLabel->setPixmap(img);

  dialog.setWindowTitle(profile->name());
  if (!editable) {
    dialog.setEditable(false);
  }

  int ret = dialog.exec();
  qInfo() << ret;
  if (editable && ret) {
    QByteArray image;
    {
      QBuffer buffer(&image);
      buffer.open(QIODevice::WriteOnly);
      dialog.ui->imgLabel->pixmap()->save(&buffer, "PNG");
    }
    profile->setImg(image);
    profile->setLocation(dialog.ui->locationEdit->text());
    profile->setTimezone(dialog.ui->timezoneBox->currentText());
  }
}

void ProfileDialog::setEditable(bool editable) {
  if (!editable) {
    ui->buttonBox->clear();
    ui->buttonBox->addButton(QDialogButtonBox::Close);
    ui->locationEdit->setEnabled(false);
    ui->timezoneBox->setEnabled(false);
    ui->imgUploadButton->hide();
  }
}

void ProfileDialog::on_imgUploadButton_clicked() {
  QString path = QFileDialog::getOpenFileName(this, tr("Open Image"), ".", tr("Images (*.png)"));
  if (path.isEmpty() || path.isNull()) {
    qDebug() << "Upload image cancelled";
    return;
  }

  QPixmap img;
  {
    // This reads a file from the resources.
    // See Qt documentation on The Qt Resource System.
    QFile imageFile(path);
    if (imageFile.open(QIODevice::ReadOnly)) {
      img.loadFromData(imageFile.readAll());
      imageFile.close();
    }
  }
  if (img.height() < 48 || img.width() < 48) {
    QMessageBox::warning(
        this, tr("Alert"),
        tr("This image is too small, please upload an image bigger than 48px x 48px"));
    return;
  }

  int w = ui->imgLabel->maximumWidth();
  int h = ui->imgLabel->maximumHeight();
  img = img.scaled(w, h, Qt::KeepAspectRatio, Qt::SmoothTransformation);
  ui->imgLabel->setPixmap(img.scaled(w, h, Qt::KeepAspectRatio, Qt::SmoothTransformation));
}
