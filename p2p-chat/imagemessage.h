#ifndef IMAGEMESSAGE_H
#define IMAGEMESSAGE_H
#include <QImage>
#include "message.h"

/**
*  @brief The ImageMessage Class, where an ImageMessage is made.
*  @author Matthew Shinkfield
*/
class ImageMessage : public Message {
 public:
  /**
 * @brief ImageMessage
 * @param sender
 * @param img
 * @param name
 * @param timestamp
 */
  ImageMessage(const QString &sender, const QByteArray &img, const QString &name,
               const QDateTime &timestamp = QDateTime::currentDateTime());
  /**
  * @brief data convert data to the format required by the network.
  * @return a QByteArray containing a network compatible representation of this TextMessage.
  */
  QByteArray data() const override;  // using override here is best practice.

  /**
  * @brief img retrieve the image data
  * @return a QByteArray containing the image data
  */
  const QByteArray &img() const;
  /**
   * @brief name retrieve the name of the image
   * @return a QString containing the image name
   */
  const QString &name() const;

 private:
  QByteArray _img;
  QString _name;
};

#endif  // IMAGEMESSAGE_H
