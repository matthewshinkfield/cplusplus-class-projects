// Author: Matthew Shinkfield
// Generated: 08 Oct 2017 @ 21:09:15
#include "filemessage.h"

FileMessage::FileMessage(const QString &sender, const QByteArray &file, const QString &name,
                         const QDateTime &timestamp)
    : Message{sender, timestamp}, _file{file}, _name{name} {
}

QByteArray FileMessage::data() const {
  // The data across the network must start with the type identifier
  QByteArray data = QByteArray::number(Message::FileMessage);
  // then a separator
  data += Message::Separator;
  data += _name.toUtf8();
  data += Message::Separator;
  // Then the data required for the message type.
  data += _file;  // Use QString::toUtf8() to encode all strings for sending.
  return data;
}

const QByteArray &FileMessage::file() const {
  return _file;
}

const QString &FileMessage::name() const {
  return _name;
}
