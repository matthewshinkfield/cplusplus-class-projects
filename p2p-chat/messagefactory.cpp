#include "messagefactory.h"
#include <QDebug>
#include <QFile>
#include <QImage>
#include <QtWidgets>
#include <iostream>
#include "actionmessage.h"
#include "filemessage.h"
#include "identitymessage.h"
#include "imagemessage.h"
#include "privatemessage.h"
#include "textmessage.h"

// NOTE: include all your message type headers here.

MessageFactory::MessageFactory() {
}

QSharedPointer<Message> MessageFactory::create(const QString &sender, const QByteArray &data,
                                               int type) const {
  int index = data.indexOf('|');
  switch (type) {
    case Message::TextMessage: {
      QString message = QString::fromUtf8(data);
      // As with std::shared_ptr, a QSharedPointer to a base class can hold a derived class pointer.
      return QSharedPointer<Message>(new TextMessage(sender, message));
      break;
    }
    case Message::IdentityMessage: {
      QString name = QString::fromUtf8(data.left(index));

      int prevInd = index + 1;
      index = data.indexOf("|", prevInd);
      QString location = QString::fromUtf8(data.mid(prevInd, index - prevInd));

      prevInd = index + 1;
      index = data.indexOf("|", prevInd);
      QString timezone = QString::fromUtf8(data.mid(prevInd, index - prevInd));

      QByteArray img = data.mid(index + 1, data.size() - index);

      return QSharedPointer<Message>(new IdentityMessage(sender, name, location, timezone, img));
      break;
    }
    case Message::FileMessage: {
      int index = data.indexOf('|');
      QString name = QString::fromUtf8(data.left(index));
      QByteArray file = data.mid(index + 1);
      return QSharedPointer<Message>(new FileMessage(sender, file, name));
      break;
    }
    case Message::ActionMessage: {
      QString name = QString::fromUtf8(data.left(index));
      QStringList operands = QString::fromUtf8(data.mid(index + 1)).split('|');
      return QSharedPointer<Message>(new ActionMessage(sender, operands, name));
      break;
    }
    case Message::PrivateMessage: {
      QString name = QString::fromUtf8(data.left(index));
      QString message = QString::fromUtf8(data.mid(index + 1));
      return QSharedPointer<Message>(new PrivateMessage(sender, message, name));
      break;
    }
    case Message::ImageMessage: {
      int index = data.indexOf('|');
      QString name = QString::fromUtf8(data.left(index));
      QByteArray img = data.mid(index + 1);
      return QSharedPointer<Message>(new ImageMessage(sender, img, name));
      break;
    }
  }
  return nullptr;
}

void MessageFactory::runTest() {
  QByteArray imageData;
  {
    // This reads a file from the resources.
    // See Qt documentation on The Qt Resource System.
    QFile imageFile(":/test/unisa-48px.png");
    if (imageFile.open(QIODevice::ReadOnly)) {
      imageData = imageFile.readAll();
      imageFile.close();
    }
  }
  QByteArray fileData;
  {
    // This reads a file from the resources.
    // See Qt documentation on The Qt Resource System.
    QFile file(":/test/Helpful Links.docx");
    if (file.open(QIODevice::ReadOnly)) {
      fileData = file.readAll();
      file.close();
    }
  }

  // Example data for each message type note you should always use the defined constants, not hard
  // coded strings for generating these (see TextMessage).
  QByteArray textMessageData = QByteArrayLiteral("1|Hello World!");
  QByteArray identityMessageData = QByteArrayLiteral("0|David|Adelaide|+930|") + imageData;
  QByteArray imageMessageData = QByteArrayLiteral("4|unisa.png|") + imageData;
  QByteArray fileMessageData = QByteArrayLiteral("3|Helpful Links.docx|") + fileData;
  QByteArray actionMessageData = QByteArrayLiteral("2|JOIN|David@10.1.1.10");
  QByteArray privateMessageData = QByteArrayLiteral("5|David@10.1.1.10|Hi, David!");

  MessageFactory factory;

  QByteArray data = stripTypeId(textMessageData);
  QSharedPointer<Message> textMessage = factory.create("none", data, Message::TextMessage);
  data = stripTypeId(identityMessageData);
  QSharedPointer<Message> identityMessage = factory.create("none", data, Message::IdentityMessage);
  data = stripTypeId(imageMessageData);
  QSharedPointer<Message> imageMessage = factory.create("none", data, Message::ImageMessage);
  data = stripTypeId(fileMessageData);
  QSharedPointer<Message> fileMessage = factory.create("none", data, Message::FileMessage);
  data = stripTypeId(actionMessageData);
  QSharedPointer<Message> actionMessage = factory.create("none", data, Message::ActionMessage);
  data = stripTypeId(privateMessageData);
  QSharedPointer<Message> privateMessage = factory.create("none", data, Message::PrivateMessage);

  // Without a complete implementation of message types, only test can be to see if a valid pointer
  // was created by MessageFactory::create();

  //  qDebug() << "TEXT MESSAGE: " << textMessage->data();
  //  qDebug() << "IDENTITY MESSAGE: " << identityMessage->data();
  //  qDebug() << "IMAGE MESSAGE: " << imageMessage->data();
  //  qDebug() << "FILE MESSAGE: " << fileMessage->data();
  //  qDebug() << "ACTION MESSAGE: " << actionMessage->data();
  //  qDebug() << "PRIVATE MESSAGE: " << privateMessage->data();

  if (textMessage == nullptr) {
    qDebug() << "TextMessage is not implemented in MessageFactory::create().";
  }
  if (identityMessage == nullptr) {
    qDebug() << "IdentityMessage is not implemented in MessageFactory::create().";
  }
  if (imageMessage == nullptr) {
    qDebug() << "ImageMessage is not implemented in MessageFactory::create().";
  }
  if (fileMessage == nullptr) {
    qDebug() << "FileMessage is not implemented in MessageFactory::create().";
  }
  if (actionMessage == nullptr) {
    qDebug() << "ActionMessage is not implemented in MessageFactory::create().";
  }
  if (privateMessage == nullptr) {
    qDebug() << "PrivateMessage is not implemented in MessageFactory::create().";
  }
}

QByteArray MessageFactory::stripTypeId(const QByteArray &array) {
  int pos = array.indexOf('|');
  return array.mid(pos + 1);
}
