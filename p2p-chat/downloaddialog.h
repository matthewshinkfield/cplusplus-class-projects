#ifndef DOWNLOADDIALOG_H
#define DOWNLOADDIALOG_H

#include <QDialog>
#include <QTreeWidgetItem>

namespace Ui {
class DownloadDialog;
}

/**
 * @brief The DownloadDialog class Where you can select a file/image to download.
 * @author Matthew Shinkfield
 */
class DownloadDialog : public QDialog {
  Q_OBJECT

 public:
  /**
 * @brief DownloadDialog Constructor
 * @param parent
 */
  explicit DownloadDialog(QWidget *parent = 0);
  ~DownloadDialog();

 public slots:
  /**
   * @brief displayDownloads display a list of downloads
   * @param parent for the window so it
   * @param list
   * @param path
   * @return if save is clicked, true. if cancel/closed, false.
   */
  static bool displayDownloads(QWidget *parent, std::map<QString, QStringList> list, QString &path);

 private slots:
  /**
   * @brief on_treeWidget_currentItemChanged If item is a top level item, it is not a file so it is
   * disallowed from being sent to the main application
   * @param current
   */
  void on_treeWidget_currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *);

  /**
   * @brief on_treeWidget_itemDoubleClicked When item is double clicked, it returns that object to
   * the main application
   * @param item
   */
  void on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item, int);

 private:
  Ui::DownloadDialog *ui;
};

#endif  // DOWNLOADDIALOG_H
