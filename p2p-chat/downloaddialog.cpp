#include "downloaddialog.h"
#include <QPushButton>
#include "chatwindow.h"
#include "ui_downloaddialog.h"

DownloadDialog::DownloadDialog(QWidget *parent) : QDialog(parent), ui(new Ui::DownloadDialog) {
  ui->setupUi(this);
}

DownloadDialog::~DownloadDialog() {
  delete ui;
}

bool DownloadDialog::displayDownloads(QWidget *parent, std::map<QString, QStringList> list,
                                      QString &path) {
  DownloadDialog dialog(parent);
  dialog.setWindowTitle("Downloads");
  for (auto const &x : list) {
    QTreeWidgetItem *topLevel = new QTreeWidgetItem();
    topLevel->setText(0, x.first);
    QList<QTreeWidgetItem *> children;
    for (QString child : x.second) {
      QTreeWidgetItem *c = new QTreeWidgetItem();
      c->setText(0, child);
      children.append(c);
    }
    topLevel->addChildren(children);
    dialog.ui->treeWidget->addTopLevelItem(topLevel);
  }
  int ret = dialog.exec();
  if (ret) {
    QTreeWidgetItem *item = dialog.ui->treeWidget->currentItem();
    QTreeWidgetItem *parent = item->parent();
    path += parent->text(0);
    path += "/";
    path += item->text(0);
    return true;
  }
  return false;
}

void DownloadDialog::on_treeWidget_currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *) {
  QPushButton *saveButton = ui->buttonBox->button(QDialogButtonBox::Save);
  if (current->parent() == nullptr) {
    saveButton->setEnabled(false);
  } else {
    saveButton->setEnabled(true);
  }
}

void DownloadDialog::on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item, int) {
  if (item->childCount() == 0) {
    this->accept();
  }
}
