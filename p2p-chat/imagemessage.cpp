// Author: Matthew Shinkfield
// Generated: 08 Oct 2017 @ 21:08:38
#include "imagemessage.h"
#include <QBuffer>
ImageMessage::ImageMessage(const QString &sender, const QByteArray &img, const QString &name,
                           const QDateTime &timestamp)
    : Message{sender, timestamp}, _img{img}, _name{name} {
}

QByteArray ImageMessage::data() const {
  // The data across the network must start with the type identifier
  QByteArray data = QByteArray::number(Message::ImageMessage);
  // then a separator
  data += Message::Separator;
  // Then the data required for the message type.
  data += _name.toUtf8();  // Use QString::toUtf8() to encode all strings for sending.
  data += Message::Separator;
  data += _img;
  return data;
}

const QByteArray &ImageMessage::img() const {
  return _img;
}

const QString &ImageMessage::name() const {
  return _name;
}
