#ifndef ACTIONMESSAGE_H
#define ACTIONMESSAGE_H
#include "message.h"

/**
*  @brief The ActionMessage class, where an ActionMessage is made.
*  @author Matthew Shinkfield
*/
class ActionMessage : public Message {
 public:
  /**
 * @brief ActionMessage Constructor
 * @param sender
 * @param operands
 * @param action
 * @param timestamp
 */
  ActionMessage(const QString &sender, const QStringList &operands, const QString &action,
                const QDateTime &timestamp = QDateTime::currentDateTime());
  /**
   * @brief data convert data to the format required by the network.
   * @return a QByteArray containing a network compatible representation of this TextMessage.
   */
  QByteArray data() const override;  // using override here is best practice.

  /**
   * @brief operands the operands of the ActionMessage
   * @return a QStringList with the operands of the action message
   */
  const QStringList &operands() const;

  /**
   * @brief action the type of the ActionMessage
   * @return a QString containing the type of action of the action message
   */
  const QString &action() const;

 private:
  QStringList _operands;
  QString _action;
};

#endif  // ACTIONMESSAGE_H
