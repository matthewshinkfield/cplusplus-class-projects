#ifndef IDENTITYMESSAGE_H
#define IDENTITYMESSAGE_H
#include <QImage>
#include "message.h"

/**
*  @brief The IdentityMessage Class, where an IdentityMessage is made.
*  @author Matthew Shinkfield
*/
class IdentityMessage : public Message {
 public:
  /**
 * @brief IdentityMessage Constructor
 * @param sender
 * @param name
 * @param location
 * @param timezone
 * @param img
 * @param timestamp
 */
  IdentityMessage(const QString &sender, const QString &name, const QString &location,
                  const QString &timezone, const QByteArray &img,
                  const QDateTime &timestamp = QDateTime::currentDateTime());
  /**
   * @brief data convert data to the format required by the network.
   * @return a QByteArray containing a network compatible representation of this TextMessage.
   */
  QByteArray data() const override;  // using override here is best practice.

  /**
   * @brief name retrieve the name of user
   * @return a QString containing the name of the user
   */
  const QString &name() const;
  /**
   * @brief location retrieve the location of user
   * @return a QString containing the location of the user
   */
  const QString &location() const;
  /**
   * @brief timezone retrieve timezone of user
   * @return a QString containing the timezone of the user
   */
  const QString &timezone() const;

  /**
   * @brief img retrieve users image data
   * @return a QByteArray containing the data of the image of the user
   */
  const QByteArray &img() const;

 private:
  QString _name;
  QString _location;
  QString _timezone;
  QByteArray _img;
};

#endif  // IDENTITYMESSAGE_H
