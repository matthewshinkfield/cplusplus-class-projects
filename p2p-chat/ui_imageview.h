/********************************************************************************
** Form generated from reading UI file 'imageview.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_IMAGEVIEW_H
#define UI_IMAGEVIEW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>

QT_BEGIN_NAMESPACE

class Ui_ImageView
{
public:
    QHBoxLayout *horizontalLayout;
    QLabel *imageLabel;

    void setupUi(QDialog *ImageView)
    {
        if (ImageView->objectName().isEmpty())
            ImageView->setObjectName(QStringLiteral("ImageView"));
        ImageView->setWindowModality(Qt::ApplicationModal);
        ImageView->resize(500, 500);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ImageView->sizePolicy().hasHeightForWidth());
        ImageView->setSizePolicy(sizePolicy);
        horizontalLayout = new QHBoxLayout(ImageView);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        imageLabel = new QLabel(ImageView);
        imageLabel->setObjectName(QStringLiteral("imageLabel"));
        imageLabel->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(imageLabel);


        retranslateUi(ImageView);

        QMetaObject::connectSlotsByName(ImageView);
    } // setupUi

    void retranslateUi(QDialog *ImageView)
    {
        ImageView->setWindowTitle(QApplication::translate("ImageView", "Dialog", Q_NULLPTR));
        imageLabel->setText(QApplication::translate("ImageView", "TextLabel", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ImageView: public Ui_ImageView {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_IMAGEVIEW_H
