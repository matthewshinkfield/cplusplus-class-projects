#include <iostream>
#include "printdetails.h"

printdetails::printdetails()
{
    printMyDetails();
    getNumber();
    for (int i=0;i<4;i++)
        std::cout << getNumber() << " Thanks for Playing" << std::endl;
}


void printdetails::printMyDetails() {
    std::cout << "Name: Matthew Shinkfield" << std::endl
              << "Email: shimy021@mymail.unisa.edu.au" << std::endl;
}

int printdetails::getNumber() {
    static int number = -1;
    ++number;
    return number;
}

