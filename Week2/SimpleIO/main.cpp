#include <iostream>
#include <cctype>
#include <ctime>
#include <random>
#include "printdetails.h"

int main()
{

    //Initial Text
    std::cout << "Welcome to Software Development with C++" << std::endl
              << "Would you like to play a game?: (y/n) ";

    //Input
    char input;
    std::cin >> input;
    //How Many Guesses
    int highScore = std::numeric_limits<int>::max();
    //If User Presses Y
    if (tolower(input) == 'y') {
        do {
            std::cout << "Try to guess how many fingers I'm holding up: ";

            //Random Number
            std::mt19937 mtRand(time(0));
            int fingers = mtRand() % 10;

            //Guess
            int guess;
            std::cin >> guess;

            //How Many Guesses
            int guessCount = 1;


            //While The User Hasn't Correctly Guesses
            while (guess != fingers) {

                if (guess < fingers) { //If The Guess Was Lower
                    std::cout << "More than that." << std::endl;
                } else if (guess > fingers) { //If The Guess Was Higher
                    std::cout << "Less than that." << std::endl;
                }

                std::cout << "Have another guess: ";

                //Clear Input to Stop Repeating
                std::cin.clear();

                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                std::cin >> guess;
                ++guessCount;
            }

            //If Correct and was first guess, else if was less than 5, otherwise.
            std::cout << "You guessed correctly, "
                      << ((guessCount == 1) ? "on the first try!":
                                              (guessCount < 5) ? "well done." : "pity it took so long.")
                      << std::endl;

            if (guessCount < highScore){
                std::cout << "New High Score!" << std::endl;
                highScore = guessCount;
            } else {
                std::cout << "Unfortunately no high score this time." << std::endl;
            }
            std::cout << "Your Score: " << guessCount << ". High Score: " << highScore << std::endl;

            std::cout << "Play Again?: (y/n) ";
            std::cin >> input;
        } while (tolower(input) == 'y');
    }
    printdetails();
    return 0;
}
