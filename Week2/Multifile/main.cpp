#include <iostream>
#include "myfunctions.h"

int main()
{
    std::string diceRoll;
    std::string lastDiceRoll;
    bool allSame = true;
    for (int i=0;i<6;i++){
        diceRoll = rollADie();
        std::cout << "Die Roll: " << diceRoll << std::endl;

        if (lastDiceRoll == ""){
            lastDiceRoll = diceRoll;
        } else {
            if (lastDiceRoll != diceRoll){
                allSame = false;
            }
            lastDiceRoll = diceRoll;
        }
    }

    if (allSame){
        std::cout << "YAHTZEE!" <<std::endl;
    }

    return 0;
}
