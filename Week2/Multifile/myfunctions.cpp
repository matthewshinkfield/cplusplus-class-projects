#include "myfunctions.h"
#include <random>


std::string rollADie() {
    static std::random_device random;
    switch (rand() % 6) {
    case 0:
        return "One";
        break;
    case 1:
        return "Two";
        break;
    case 2:
        return "Three";
        break;
    case 3:
        return "Four";
        break;
    case 4:
        return "Five";
        break;
    case 5:
        return "Six";
    }
    return "Broken Die!";
}
