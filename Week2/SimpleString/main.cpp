#include <iostream>
#include <limits>
#include <string>
#include <vector>

/**
* @brief clearInputBuffer removes all characters up to and including a newline
* character from the input buffer.
*/
void clearInputBuffer() {
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

/**
* @brief reverseWords takes a sentence and changes the order of the words.
* @param input a sentence
* @return the sentence with the word order reversed.
*/
std::string reverseWords(const std::string &input) {
    std::string in = input;
    std::vector<std::string> words;
    std::string result = "";
    size_t pos = 0;
    while ((pos = in.find(" ")) != std::string::npos){
        words.push_back(" " +in.substr(0,pos));
        in.erase(0,pos+1);
    }
    result = in;
    for (int i = words.size(); i-- > 0;){
        result.append(words[i]);
    }
    return result;
}

int main()
{
    std::string name;
    std::cout << "Tell me your first name: ";
    std::cin >> name;
    std::cout << "Hello, " << name << " it's nice to meet you!" << std::endl;
    std::cout << "Tell me something about yourself (more than one word): ";
    std::string aboutYou;
    clearInputBuffer();
    getline(std::cin, aboutYou);
    std::cout << name << " told me: \"" << aboutYou << "\"" << std::endl;
    std::string reversed = reverseWords(aboutYou);
    std::cout << reversed << std::endl;
}
