#ifndef CAT_H
#define CAT_H
#include "animal.h"

/**
*  @author Matthew Shinkfield
*  @file cat.h
*  @date 02 Sep 2017
*/
class Cat : public Animal
{
public:
    ~Cat() {}
    Cat( const std::string& colour, const std::string& name, const bool isPet );


    const std::shared_ptr< std::string > sound() const;
    void setSound( std::shared_ptr< std::string > strng );

private:
    std::shared_ptr< std::string > _sound;
};

#endif // CAT_H
