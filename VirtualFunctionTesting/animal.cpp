// Author: Matthew Shinkfield
// Generated: 02 Sep 2017 @ 14:03:41
#include "animal.h"

const std::string& Animal::colour() const
{
    return _colour;
}

const std::string& Animal::name() const
{
    return _name;
}

bool Animal::isPet() const
{
    return _isPet;
}

Animal::Animal( const std::string& colour, const std::string& name, const bool isPet )
: _colour{colour}, _name{name}, _isPet{isPet}
{
}
