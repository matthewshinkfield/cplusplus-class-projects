#ifndef DOG_H
#define DOG_H
#include "animal.h"

/**
*  @author Matthew Shinkfield
*  @file dog.h
*  @date 02 Sep 2017
*/
class Dog : public Animal
{
public:
    Dog( const std::string& colour, const std::string& name, const bool isPet );
    const std::shared_ptr< std::string > sound() const;
    void setSound( std::shared_ptr< std::string > strng );

private:
    std::shared_ptr< std::string > _sound;
};

#endif // DOG_H
