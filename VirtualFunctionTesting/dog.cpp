// Author: Matthew Shinkfield
// Generated: 02 Sep 2017 @ 14:03:49
#include "dog.h"


Dog::Dog( const std::string& colour, const std::string& name, const bool isPet )
: Animal{colour, name, isPet}
{
}

const std::shared_ptr< std::string > Dog::sound() const
{
    return _sound;
}


void Dog::setSound( std::shared_ptr< std::string > strng )
{
    _sound = strng;
}
