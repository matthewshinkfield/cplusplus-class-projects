// Author: Matthew Shinkfield
// Generated: 02 Sep 2017 @ 14:04:00
#include "cat.h"

Cat::Cat( const std::string& colour, const std::string& name, const bool isPet )
: Animal{colour, name, isPet}
{
}

const std::shared_ptr< std::string > Cat::sound() const
{
    return _sound;
}

void Cat::setSound( std::shared_ptr< std::string > strng )
{
    _sound = strng;
}
