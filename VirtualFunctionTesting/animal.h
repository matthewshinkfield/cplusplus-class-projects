#ifndef ANIMAL_H
#define ANIMAL_H
#include <string>
#include <map>
#include <memory>

/**
*  @author Matthew Shinkfield
*  @file animal.h
*  @date 02 Sep 2017
*/
class Animal
{
public:
    //    virtual ~Animal() {}

    const std::string& colour() const;
    const std::string& name() const;
    bool isPet() const;

    virtual const std::shared_ptr< std::string > sound() const = 0;
    virtual void setSound( std::shared_ptr< std::string > strng ) = 0;

protected:
    Animal( const std::string& colour, const std::string& name, bool isPet );

private:
    std::string _colour;
    std::string _name;
    bool _isPet;
};

#endif // ANIMAL_H
