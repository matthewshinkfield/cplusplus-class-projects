// Author: Matthew Shinkfield
// Generated: 06 Sep 2017 @ 23:13:30
#include "phone.h"
#include <iomanip>
#include <iostream>

Phone::Phone(const std::string &id, const std::string &brand, const std::string &model,
             double purchasePrice, const Date &purchaseDate,
             const std::string &serialNumber, const std::string &operatingSystem,
             const std::string &billingIdentifier, const std::string &phoneNumber)
    : Asset{id, brand, model, purchasePrice, purchaseDate},
    _serialNumber{serialNumber}, _operatingSystem{operatingSystem},
    _billingIdentifier{billingIdentifier}, _phoneNumber{phoneNumber}
{
    setLifeSpan(2.0);
}

std::string Phone::serialNumber() const
{
    return _serialNumber;
}

void Phone::setSerialNumber(std::string &serialNumber)
{
    _serialNumber = serialNumber;
}

const std::string &Phone::operatingSystem() const
{
    return _operatingSystem;
}

void Phone::setOperatingSystem(std::string &operatingSystem)
{
    _operatingSystem = operatingSystem;
}

const std::string &Phone::phoneNumber() const
{
    return _phoneNumber;
}

void Phone::setPhoneNumber(std::string &phoneNumber)
{
    _phoneNumber = phoneNumber;
}

const std::string &Phone::billingIdentifier() const
{
    return _billingIdentifier;
}

void Phone::setBillingIdentifier(std::string &billingIdentifier)
{
    _billingIdentifier = billingIdentifier;
}

const std::shared_ptr<Custodian> Phone::custodian() const
{
    return _custodian;
}

void Phone::setCustodian(std::shared_ptr<Custodian> custodian)
{
    _custodian = custodian;
}

void Phone::print(std::ostream &strm) const
{
    strm << std::setw(20) << "Serial Number:" << _serialNumber << std::endl
         << std::setw(20) << "Operating System:" << _operatingSystem << std::endl
         << std::setw(20) << "Phone Number:" << _phoneNumber << std::endl
         << std::setw(20) << "Billing ID:" << _billingIdentifier;
}
