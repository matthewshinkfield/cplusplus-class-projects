#include "computer.h"
#include <iomanip>
#include <iostream>

Computer::Computer(const std::string &id, const std::string &brand,
                   const std::string &model, double purchasePrice,
                   const Date &purchaseDate, const std::string &serialNumber,
                   const std::string &operatingSystem,
                   const std::string &networkIdentifier, const bool laptop)
    : Asset{id, brand, model, purchasePrice, purchaseDate},
    _serialNumber{serialNumber}, _operatingSystem{operatingSystem},
    _networkIdentifier{networkIdentifier}
{
    if (laptop) {
          setLifeSpan(3.0);
      } else {
          setLifeSpan(4.0);
      }
}

Computer::Computer(const std::string &id, const std::string &brand,
                   const std::string &model, double purchasePrice,
                   const Date &purchaseDate, const std::string &serialNumber,
                   const std::string &operatingSystem, bool laptop)
    : Asset{id, brand, model, purchasePrice, purchaseDate},
    _serialNumber{serialNumber},
    _operatingSystem{operatingSystem}, _networkIdentifier{""}, _custodian{nullptr}
{
    if (laptop) {
          setLifeSpan(3.0);
      } else {
          setLifeSpan(4.0);
      }
}

std::string Computer::serialNumber() const
{
    return _serialNumber;
}

void Computer::setSerialNumber(std::string &serialNumber)
{
    _serialNumber = serialNumber;
}

const std::string &Computer::operatingSystem() const
{
    return _operatingSystem;
}

void Computer::setOperatingSystem(std::string &operatingSystem)
{
    _operatingSystem = operatingSystem;
}

const std::string &Computer::networkIdentifier() const
{
    return _networkIdentifier;
}

void Computer::setNetworkIdentifier(std::string &networkIdentifier)
{
    _networkIdentifier = networkIdentifier;
}

const std::shared_ptr<Custodian> Computer::custodian() const
{
    return _custodian;
}

void Computer::print(std::ostream &strm) const
{
    strm << std::setw(20) << "Serial Number:" << _serialNumber << std::endl
         << std::setw(20) << "Network Identifier:" << _networkIdentifier
         << std::endl
         << std::setw(20) << "Operating System:" << _operatingSystem;
}

void Computer::setCustodian(std::shared_ptr<Custodian> custodian)
{
    _custodian = custodian;
}
