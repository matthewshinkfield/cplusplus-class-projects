TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    date.cpp \
    assetregister.cpp \
    custodian.cpp \
    maintenance.cpp \
    asset.cpp \
    computer.cpp \
    phone.cpp \
    television.cpp \
    hmd.cpp \
    asssettesting.cpp\
    menuinterface.cpp

HEADERS += date.h \
    assetregister.h \
    custodian.h \
    maintenance.h \
    asset.h \
    computer.h \
    phone.h \
    television.h \
    hmd.h \
    assettesting.h \
    testingsetting.h \
    menuinterface.h
