#ifndef HMD_H
#define HMD_H
#include "asset.h"

/**
 *   @brief HMD in the asset register
 */
class HMD : public Asset
{
public:
    /**
     * @brief HMD HMD Constructor
     * @param id
     * @param brand
     * @param model
     * @param purchasePrice
     * @param purchaseDate
     * @param serialNumber
     */
    HMD(const std::string &id, const std::string &brand, const std::string &model,
        double purchasePrice, const Date &purchaseDate,
        const std::string &serialNumber);

    /**
     * @brief serialNumber retrieve serial number
     * @return _serialNumber serial number of HMD
     */
    std::string serialNumber() const;
    /**
     * @brief setSerialNumber set serial number
     * @param serialNumber serial number for HMD
     */
    void setSerialNumber(std::string &serialNumber);

    /** @brief custodian retrieve custodian
     * @return _custodian of HMD, nullptr if does not exist
     */
    const std::shared_ptr<Custodian> custodian() const;
    /**
     * @brief setCustodian Set custodian
     * @param custodian Custodian for HMD
     */
    void setCustodian(std::shared_ptr<Custodian> cust);
    /**
     * @brief print print data from HMD
     * @param strm ostream for easy string merging
     */
    void print(std::ostream &strm) const;
private:
    std::string _serialNumber; /**< serial number for Phone */
    std::shared_ptr<Custodian> _custodian;/**< custodian for Phone */
};

#endif // HMD_H
