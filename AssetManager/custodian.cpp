// Author: Matthew Shinkfield
// Generated: 19 Aug 2017 @ 22:13:01
#include "custodian.h"
#include <iomanip>
#include <iostream>

Custodian::Custodian(const std::string &name, const std::string &department,
                     const std::string &phoneNumber, Date &employmentStart)
    : _name{name},
    _department{department}, _phoneNumber{phoneNumber}, _employmentStart{
                                                                         employmentStart}
{
}

const std::string &Custodian::name() const
{
    return _name;
}

void Custodian::setName(std::string &name)
{
    _name = name;
}

const std::string &Custodian::department() const
{
    return _department;
}

void Custodian::setDepartment(std::string &department)
{
    _department = department;
}

const std::string &Custodian::phoneNumber() const
{
    return _phoneNumber;
}

void Custodian::setPhoneNumber(std::string &phoneNumber)
{
    _phoneNumber = phoneNumber;
}

const Date &Custodian::employmentStart() const
{
    return _employmentStart;
}

void Custodian::setEmploymentStart(Date &employmentStart)
{
    _employmentStart = employmentStart;
}

std::ostream &operator <<(std::ostream &strm, const Custodian *c)
{
    return strm << std::setw(20) << "Name:" << std::setw(10) << c->name()
                << std::endl
                << std::setw(20) << "Department:" << std::setw(10)
                << c->department()
                << std::endl
                << std::setw(20) << "Phone Number:" << std::setw(10)
                << c->phoneNumber()
                << std::endl
                << std::setw(20) << "Employement Start:" << std::setw(10)
                << c->employmentStart();
}
