// Author: Matthew Shinkfield
// Generated: 06 Sep 2017 @ 23:13:52
#include "hmd.h"
#include <iomanip>
#include <iostream>

HMD::HMD(const std::string &id, const std::string &brand, const std::string &model,
         double purchasePrice, const Date &purchaseDate, const std::string &serialNumber)
    : Asset{id, brand, model, purchasePrice, purchaseDate},
    _serialNumber{serialNumber}
{
    setLifeSpan(2.5);
}

std::string HMD::serialNumber() const
{
    return _serialNumber;
}

void HMD::setSerialNumber(std::string &serialNumber)
{
    _serialNumber = serialNumber;
}

const std::shared_ptr<Custodian> HMD::custodian() const
{
    return _custodian;
}

void HMD::setCustodian(std::shared_ptr<Custodian> cust)
{
    _custodian = cust;
}

void HMD::print(std::ostream &strm) const
{
    strm << std::setw(20) << "Serial Number:" << _serialNumber;
}
