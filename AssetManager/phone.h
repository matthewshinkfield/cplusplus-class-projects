#ifndef PHONE_H
#define PHONE_H
#include "asset.h"

/**
 *  @brief Phone in the asset register
 */
class Phone : public Asset
{
public:
    /**
     * @brief Phone Phone Constructor
     * @param id
     * @param brand
     * @param model
     * @param purchasePrice
     * @param purchaseDate
     * @param serialNumber
     * @param operatingSystem
     * @param billingIdentifier
     * @param phoneNumber
     */
    Phone(const std::string &id, const std::string &brand, const std::string &model,
          double purchasePrice, const Date &purchaseDate,
          const std::string &serialNumber, const std::string &operatingSystem,
          const std::string &billingIdentifier, const std::string &phoneNumber);

    /**
     * @brief serialNumber retrieve serial number
     * @return _serialNumber serial number of Phone
     */
    std::string serialNumber() const;
    /**
     * @brief setSerialNumber set serial number
     * @param serialNumber serial number for Phone
     */
    void setSerialNumber(std::string &serialNumber);
    /**
     * @brief operatingSystem retrieve operating system
     * @return operatingSystem operating system of Phone
     */
    const std::string &operatingSystem() const;
    /**
     * @brief setOperatingSystem set operating system
     * @param operatingSystem operating system for Phone
     */
    void setOperatingSystem(std::string &operatingSystem);
    /**
     * @brief phoneNumber retrieve phone number
     * @return _phoneNumber phone number of Asset
     */
    const std::string &phoneNumber() const;
    /**
     * @brief setPhoneNumber set phone number
     * @param phoneNumber phone number for phone
     */
    void setPhoneNumber(std::string &phoneNumber);
    /**
     * @brief billingIdentifier retrieve billing identifier
     * @return _billingIdentifier biiling identifier of Phone
     */
    const std::string &billingIdentifier() const;
    /**
     * @brief setBillingIdentifier set billing identifier
     * @param billingIdentifier billing identifier for Phone
     */
    void setBillingIdentifier(std::string &billingIdentifier);

    /** @brief custodian retrieve custodian
     * @return _custodian of Phone, nullptr if does not exist
     */
    const std::shared_ptr<Custodian> custodian() const;
    /**
     * @brief setCustodian Set custodian
     * @param custodian Custodian for HMD
     */
    void setCustodian(std::shared_ptr<Custodian> cust);

    /**
     * @brief print print data from Phone
     * @param strm ostream for easy string merging
     */
    void print(std::ostream &strm) const;
private:
    std::string _serialNumber; /**< serial number for Phone */
    std::string _operatingSystem; /**< operating system for Phone */
    std::string _billingIdentifier; /**< billing identifier for Phone */
    std::string _phoneNumber; /**< phone number for Phone */
    std::shared_ptr<Custodian> _custodian; /**< Custodian for Phone */
};
#endif // PHONE_H
