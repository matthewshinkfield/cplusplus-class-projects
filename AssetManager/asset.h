#ifndef ASSET_H
#define ASSET_H
#include <string>
#include <map>
#include <memory>
#include <vector>
#include <iostream>
#include "maintenance.h"
#include "custodian.h"
#include "date.h"

/**
 * @brief The Asset class the abstract base class for an asset in the asset
 * register.
 */
class Asset
{
public:
    /**
     * @brief ~Asset deconstructor for Asset
     */
    virtual ~Asset() {
    }

    /**
     * @brief id retrieve the unique identifier.
     * @return _id the unique identifier of the Asset
     */
    const std::string &id() const;

    /**
     * @brief setId set the unique identifier
     * @param id unique identifier for Asset
     */
    void setId(std::string &id);

    /**
     * @brief brand retrieve the brand name
     * @return _brand brand name of the brand of the Asset.
     */
    const std::string &brand() const;
    /**
     * @brief setBrand set the brand name
     * @param brand brand name for Asset
     */
    void setBrand(std::string &brand);
    /**
     * @brief model retrieve the model number
     * @return _model model number of the Asset.
     */
    const std::string &model() const;
    /**
     * @brief setModel set the model number
     * @param model model number for Asset
     */
    void setModel(std::string &model);

    /**
     * @brief purchasePrice retrieve purchase price
     * @return _purchasePrice purchase price of Asset
     */
    double purchasePrice() const;
    /**
     * @brief setPurchasePrice set the purchase price
     * @param purchasePrice purchase price for Asset
     */
    void setPurchasePrice(double purchasePrice);

    /**
     * @brief purchaseDate retrieve purchase date
     * @return _purchaseDate purchase date of Asset.
     */
    const Date &purchaseDate() const;
    /**
     * @brief setPurchaseDate set the purchase date
     * @param purchaseDate purchase date for Asset
     */
    void setPurchaseDate(Date &purchaseDate);

    /**
     * @brief disposalDate retrieve disposal date
     * @return _disposalDate the date which the Asset is di
     */
    const Date &disposalDate() const;

    /**
     * @brief setDisposalDate set disposal date
     * @param date disposal date for Asset
     */
    void setDisposalDate(Date date);

    /**
     * @brief lifeSpan retrieve life span
     * @return _lifeSpan lifespan of Asset
     */
    double lifeSpan() const;

    /**
     * @brief setLifeSpan set the life span
     * @param lifeSpan life span of Asset
     */
    void setLifeSpan(double lifeSpan);

    /**
     * @brief maintenance retrieve maintenance records
     * @return _maintenance list of Maintenance records for Asset
     */
    const std::vector<Maintenance> maintenance() const;

    /**
     * @brief addMaintenance add maintenance record
     * @param record Maintenance record to add to Asset
     */
    void addMaintenance(Maintenance record);

    /**
     * @brief serialNumber retrieve serial number
     * @return _serialNumber serial number of Asset
     */
    virtual std::string serialNumber() const = 0;

    /**
     * @brief setSerialNumber set serial number
     */
    virtual void setSerialNumber(std::string &) const {
    }

    /**
     * @brief custodian retrieve custodian
     * @return _custodian for Asset, nullptr if does not exist
     */
    virtual const std::shared_ptr<Custodian> custodian() const
    {
        return nullptr;
    }

    /**
     * @brief setCustodian set custodian
     */
    virtual void setCustodian(std::shared_ptr<Custodian> ) {
    }

    /**
     * @brief print print data from derived class
     * @param strm ostream for easy string merging
     */
    virtual void print(std::ostream &) const {
    }


    /**
     * @brief calculateDepreciation Calculate deprecation
     * @param other Date to calculate to or from
     * @return amount of depraction of Asset
     */
    double calculateDepreciation(const Date &other) const;

    /**
     * @brief dispose Dipose Asset at Date
     * @param disposalDate Date of Disposal
     */
    void dispose(Date &disposalDate);
protected:
    /**
     * @brief Asset constructor for the base class asset
     * @param id the unique identifier for the base class asset
     * @param brand the brand name for the base class asset
     * @param model the model number for the base class asset
     * @param purchasePrice the purchase price for the base class asset
     * @param purchaseDate the purchase date for the base class asset
     */
    Asset (const std::string &id, const std::string &brand, const std::string &model,
           const double purchasePrice, const Date &purchaseDate);
private:
    std::vector<Maintenance> _maintenance; /**< a list of maintenance records */

    std::string _id; /**< a unique identifier for the asset */
    std::string _brand; /**< brand name for the asset*/
    std::string _model; /**< model number for the asset */
    double _purchasePrice; /**< purchase price for the asset */
    Date _purchaseDate; /**< the date on which the asset was purchased */
    Date _disposalDate; /**< the date on which the asset was disposed */
    float _lifeSpan; /**< life span of asset, allows for one use calculateDeprecation */

    /**
     * @brief operator << overloads the stream operator.
     * @param os the output stream.
     * @param asset the Asset
     *
     */

    friend std ::ostream &operator <<(std::ostream &strm, const Asset *asset);
};

#endif // ASSET_H
