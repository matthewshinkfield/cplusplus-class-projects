#ifndef MAINTENANCE_H
#define MAINTENANCE_H
#include "date.h"

/**
 *  @brief Maintenance for Asset
 */
class Maintenance
{
public:
    /**
     * @brief Maintenance
     * @param maintenanceDate
     * @param maintainer
     */
    Maintenance(const Date &maintenanceDate, const std::string &maintainer);

    /**
     * @brief maintenanceDate
     * @return
     */
    const Date &maintenanceDate() const;
    /**
     * @brief maintainer
     * @return
     */
    const std::string &maintainer() const;


    /**
     * @brief operator == - If two maintenances are the same.
     * @param Maintenance maint2
     * @return bool
     */
    bool operator ==(const Maintenance &maint2);

    /**
     * @brief operator != - If two maintenances aren't the same.
     * @param Maintenance maint2
     * @return bool
     */
    bool operator !=(const Maintenance &maint2);
private:
    friend std ::ostream &operator <<(std::ostream &strm,
                                      const Maintenance &maintenance);
    Date _maintenanceDate;
    std::string _maintainer;
};

#endif // MAINTENANCE_H
