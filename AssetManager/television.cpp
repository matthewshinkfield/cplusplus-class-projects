// Author: Matthew Shinkfield
// Generated: 06 Sep 2017 @ 23:13:40
#include "television.h"
#include <iomanip>
#include <iostream>

Television::Television(const std::string &id, const std::string &brand,
                       const std::string &model, double purchasePrice,
                       const Date &purchaseDate, const std::string &serialNumber,
                       const std::string &location)
    : Asset{id, brand, model, purchasePrice, purchaseDate},
    _serialNumber{serialNumber}, _location{location}
{
    setLifeSpan(6);
}

std::string Television::serialNumber() const
{
    return _serialNumber;
}

void Television::setSerialNumber(std::string &serialNumber)
{
    _serialNumber = serialNumber;
}

const std::string &Television::location() const
{
    return _location;
}

void Television::setLocation(std::string &location)
{
    _location = location;
}

void Television::print(std::ostream &strm) const
{
    strm << std::setw(20) << "Serial Number:" << _serialNumber << std::endl
         << std::setw(20) << "Location:" << _location;
}
