// Author: Matthew Shinkfield
// Generated: 29 Aug 2017 @ 17:06:52
#include "maintenance.h"
#include <iostream>

Maintenance::Maintenance(const Date &maintenanceDate, const std::string &maintainer)
    : _maintenanceDate{maintenanceDate}, _maintainer{maintainer}
{
}

const Date &Maintenance::maintenanceDate() const
{
    return _maintenanceDate;
}

const std::string &Maintenance::maintainer() const
{
    return _maintainer;
}

bool Maintenance::operator ==(const Maintenance &maint2)
{
    return((_maintenanceDate.differenceInDays(maint2.maintenanceDate()) ==
            0) &&
           (_maintainer == maint2.maintainer()));
}

bool Maintenance::operator !=(const Maintenance &maint2)
{
    return !(*this == maint2);
}

std::ostream &operator <<(std::ostream &strm, const Maintenance &maintenance)
{
    return strm << maintenance.maintenanceDate().format() << " by "
                << maintenance.maintainer()
                << std::endl;
}
