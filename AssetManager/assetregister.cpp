#include "assetregister.h"
#include <algorithm>
#include "computer.h"
#include "phone.h"
#include "television.h"
#include "hmd.h"

AssetRegister &AssetRegister::instance()
{
    static AssetRegister s_instance;

    return s_instance;
} // AssetRegister::instance

std::shared_ptr<Asset> AssetRegister::retrieveAsset(const std::string &assetId)
{
    auto assetIt = _assets.find(assetId);

    if (assetIt != _assets.end()) {
          return assetIt->second;
      }
    return nullptr;
} // AssetRegister::retrieveAsset

std::vector<std::shared_ptr<Asset> > AssetRegister::retrieveAssetByType(
    const std::string &type)
{
    std::vector<std::shared_ptr<Asset> > list;
    for (auto it = _assets.begin() ; it != _assets.end() ; ++it) {
          if (type == "ALL") {
                list.push_back(it->second);
            } else if (type == "COMPUTER") {
                if (dynamic_cast<Computer *>(it->second.get())) {
                      list.push_back(it->second);
                  }
            } else if (type == "PHONE") {
                if (dynamic_cast<Phone *>(it->second.get())) {
                      list.push_back(it->second);
                  }
            } else if (type == "TELEVISION") {
                if (dynamic_cast<Television *>(it->second.get())) {
                      list.push_back(it->second);
                  }
            } else if (type == "HMD") {
                if (dynamic_cast<HMD *>(it->second.get())) {
                      list.push_back(it->second);
                  }
            } else {
                return list;
            }
      }
    return list;
} // AssetRegister::retrieveAssetByType

std::vector<std::shared_ptr<Asset> > AssetRegister::retrieveAssetByCustodian(
    const std::string &name)
{
    std::vector<std::shared_ptr<Asset> > list;
    for (auto it = _assets.begin() ; it != _assets.end() ; ++it) {
          Asset *a = it->second.get();
          if (a->custodian() == nullptr) {
                continue;
            }
          if (a->custodian().get()->name() == name) {
                list.push_back(it->second);
            }
      }
    return list;
} // AssetRegister::retrieveAssetByCustodian

std::shared_ptr<Asset> AssetRegister::retrieveAssetBySerial(const std::string &serialNum)
{
    for (auto it = _assets.begin() ; it != _assets.end() ; ++it) {
          Asset *a = it->second.get();
          if (a->serialNumber() == serialNum) {
                return it->second;
            }
      }
    return nullptr;
} // AssetRegister::retrieveAssetBySerial

bool AssetRegister::storeAsset(std::shared_ptr<Asset> asset)
{
    if (!_assets.count(asset->id())) {
          _assets[ asset->id() ] = std::shared_ptr<Asset> { asset };
          return true;
      }
    return false;
} // AssetRegister storeAsset

AssetRegister::AssetRegister()
{
}
