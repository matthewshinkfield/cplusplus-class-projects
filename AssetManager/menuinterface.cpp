#include <limits>
#include <memory>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <iomanip>
#include "menuinterface.h"
#include "computer.h"
#include "television.h"
#include "hmd.h"
#include "phone.h"

MenuInterface::MenuInterface(std::ostream &display, std::istream &input)
    : _display{display}, _input{input}
{
    /*    Computer comp{"comp003", "Dell", "Optiplex 9020", 1000.00,
                      Date{04, Date::March, 2016 }, "51NC165",
                      "Windows 10 Enterprise",
                      "DESK1001", false };
        Date date      = Date{15, Date::February, 2011 };
        Custodian cust = Custodian("David Webb", "ITMS", "8888 8888", date);

        std::shared_ptr<Custodian> custodian = std::make_shared<Custodian>(cust);
        comp.setCustodian(custodian);
        AssetRegister &am = AssetRegister::instance();
        am.storeAsset(std::make_shared<Computer>(comp));
        _display << am.retrieveAsset("comp003").get();

        _display << std::endl << std::endl << std::endl << std::endl << std::endl*/;
}

void MenuInterface::displayMainMenu() const
{
    _display << "What would you like to do?" << std::endl;
    _display << " (a)dd an asset" << std::endl;
    _display << " (d)ispose an asset" << std::endl;
    _display << " (u)pdate asset custodian or location" << std::endl;
    _display << " add asset (m)aintenance record" << std::endl;
    _display << " (l)ist assets by type" << std::endl;
    _display << " List assets by (c)ustodian" << std::endl;
    _display << " (f)ind asset" << std::endl;
    _display << " (q)uit" << std::endl;
}

void MenuInterface::assetSubMenu() const
{
    _display << "Please select asset type: " << std::endl;
    _display << " (c)omputer" << std::endl;
    _display << " (p)hone" << std::endl;
    _display << " (t)elevision" << std::endl;
    _display << " (h)dm" << std::endl;
    _display << " (b)ack to main menu" << std::endl;
}

void MenuInterface::listByAssetSubMenu() const
{
    _display << "List Asset by Type" << std::endl;
    _display << " list (a)ll assets" << std::endl;
    _display << " list (c)omputers" << std::endl;
    _display << " list (p)hones" << std::endl;
    _display << " list (t)elevision" << std::endl;
    _display << " list (h)mds" << std::endl;
    _display << " (b)ack to main menu" << std::endl;
}

void MenuInterface::findAssetSubMenu() const
{
    _display << "Find asset" << std::endl;
    _display << " search by (a)sset id" << std::endl;
    _display << " search by (s)erial" << std::endl;
    _display << " (b)ack to main menu" << std::endl;
}

char MenuInterface::getCharacterInput() const
{
    char input;

    _input >> input;
    _input.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return input;
}

bool MenuInterface::processSelection(char selection)
{
    switch (selection) {
      case 'a':
          addAsset();
          break;

      case 'd':
          disposeAsset();
          break;

      case 'u':
          updateAsset();
          break;
      case 'm':
          addMaintenance();
          break;

      case 'l':
          listAssetsByType();
          break;

      case 'c':
          listAssetsByCustodian();
          break;

      case 'f':
          findAsset();
          break;

      case 'q':
          return false;
      default:
          _display << "Sorry, \'" << selection
                   << "\' is not a valid option, please try again."
                   << std::endl;
          break;
      }
    return true;
} // MenuInterface::processSelection

void MenuInterface::addAsset()
{
    AssetRegister &am = AssetRegister::instance();
    Assets type;

    assetSubMenu();

    switch (char selection = getCharacterInput()) {
      case 'c':
          type = Assets::Computer;
          _display << "Computer asset type selected" << std::endl;
          break;

      case 'p':
          type = Assets::Phone;
          _display << "Phone asset type selected" << std::endl;
          break;

      case 't':
          type = Assets::Television;
          _display << "Television type asset selected" << std::endl;
          break;

      case 'h':
          type = Assets::HMD;
          _display << "HMD asset type selected" << std::endl;
          break;

      case 'b':
          return;

          break;

      default:
          _display << "Sorry, \'" << selection
                   << "\' is not a valid option, please try again."
                   << std::endl;
          return;

          break;
      }

    // Declare variables.
    std::string line, id, serialNumber, brand, model;
    double purchasePrice;
    int day, month, year;
    Date purchaseDate;

    // Display Basic Asset Information
    _display << "-------- Asset Information --------" << std::endl;
    // Enter ID
    _display << "Enter ID: ";
    while (id == "") {
          std::getline(_input, id);
          if (am.retrieveAsset(id) != nullptr && id != "") {
                id = "";
                _display << "Asset with this ID already exists." << std::endl;
                _display << "Enter ID: ";
            }
      }
    // Enter Brand Name
    _display << "Enter Brand Name: ";
    while (brand == "") {
          std::getline(_input, brand);
      }
    // Enter Model
    _display << "Enter Model: ";
    while (model == "") {
          std::getline(_input, model);
      }
    _display << "Enter Purchase Price: ";
    while (!(_input >> purchasePrice)) {
          _display << "Not a valid number, Enter Purchase Price: ";
          clearInputBuffer();
      }

    do {
          _display << "Enter Purchase Date[dd/mm/yyyy]: ";
          _input >> line;
          if (line.length() != 10) {
                _display << "Error: Invalid Date Format" << std::endl;
                continue;
            }
          try {
                day   = std::stoi(line.substr(0, 2));
                month = std::stoi(line.substr(3, 4));
                year  = std::stoi(line.substr(6, 9));
            }
          catch (...) {
              _display << "Error: Invalid Date Format" << std::endl;
              continue;
          }
          purchaseDate = Date{day, static_cast<Date::Month>(month - 1), year };
          if (!purchaseDate.valid()) {
                _display << "Error: Invalid Date Format" << std::endl;
            }
          clearInputBuffer();
      } while (!purchaseDate.valid());

    // Custodian Variables
    std::string custName, custDept, custPhone;
    Date employDate;

    std::shared_ptr<Custodian> custodian = nullptr;
    if (type != Assets::Television) {
          bool hasCustodian = false;
          _display << "-------- Custodian Information --------" << std::endl;

          char custodianChoice;
          do {
                _display << "Do you wish to add a Custodian [y/n]: ";
                custodianChoice = getCharacterInput();
            } while (tolower(custodianChoice) != 'y' &&
                     tolower(custodianChoice) != 'n');

          if (custodianChoice == 'y') {
                hasCustodian = true;
            }

          if (hasCustodian) {
                _display << "Enter Custodian Name: ";
                while (custName == "") {
                      std::getline(_input, custName);
                  }
                _display << "Enter Custodian Department: ";
                while (custDept == "") {
                      std::getline(_input, custDept);
                  }
                _display << "Enter Phone Number: ";
                while (custPhone == "") {
                      std::getline(_input, custPhone);
                  }
                do {
                      _display << "Enter Employment Date[dd/mm/yyyy]: ";
                      _input >> line;
                      if (line.length() != 10) {
                        _display
                                << "Error: Invalid Date Format"
                                << std::endl;
                        continue;
                    }
                      try {
                        day   = std::stoi(line.substr(0, 2));
                        month = std::stoi(line.substr(3, 4));
                        year  = std::stoi(line.substr(6, 9));
                    }
                      catch (...) {
                          _display << "Error: Invalid Date Format"
                                   << std::endl;
                          continue;
                      }
                      employDate =
                          Date{day,
                               static_cast<Date::Month>(month - 1),
                               year };
                      if (!employDate.valid()) {
                        _display
                                << "Error: Invalid Date Format"
                                << std::endl;
                    }
                      clearInputBuffer();
                  } while (!employDate.valid());

                custodian = std::make_shared<Custodian>(Custodian{custName,
                                                                  custDept,
                                                                  custPhone,
                                                                  employDate });
            }
      }

    switch (type) {
      case Assets::Computer: {
          _display << "-------- Computer Information --------" << std::endl;
          std::string operatingSystem, networkID;
          bool laptop = false;

          _display << "Enter Serial Number: ";
          while (serialNumber == "") {
                std::getline(_input, serialNumber);
            }
          _display << "Enter Operating System: ";
          while (operatingSystem == "") {
                std::getline(_input, operatingSystem);
            }
          _display << "Enter Network Identifier: ";
          while (networkID == "") {
                std::getline(_input, networkID);
            }
          char laptopChoice;
          do {
                _display << "Is This a Laptop [y/n]: ";
                laptopChoice = getCharacterInput();
            } while (tolower(laptopChoice) != 'y' &&
                     tolower(laptopChoice) != 'n');

          if (laptopChoice == 'y') {
                laptop = true;
            }
          Computer comp{id, brand, model, purchasePrice, purchaseDate,
                    serialNumber, operatingSystem, networkID, laptop };
          comp.setCustodian(custodian);
          am.storeAsset(std::make_shared<Computer>(comp));

          break;
      }

      case Assets::Phone: {
          _display << "-------- Phone Information --------" << std::endl;
          std::string operatingSystem, phoneNumber, billingIdentifier;

          _display << "Enter Serial Number: ";
          while (serialNumber == "") {
                std::getline(_input, serialNumber);
            }
          _display << "Enter Operating System: ";
          while (operatingSystem == "") {
                std::getline(_input, operatingSystem);
            }
          _display << "Enter Billing Identifier: ";
          while (billingIdentifier == "") {
                std::getline(_input, billingIdentifier);
            }
          _display << "Enter Phone Number: ";
          while (phoneNumber == "") {
                std::getline(_input, phoneNumber);
            }

          Phone phone{id, brand, model, purchasePrice, purchaseDate,
                  serialNumber,
                  operatingSystem, phoneNumber, billingIdentifier };
          phone.setCustodian(custodian);
          am.storeAsset(std::make_shared<Phone>(phone));
          break;
      }

      case Assets::Television: {
          _display << "-------- Television Information --------" << std::endl;
          std::string location;

          _display << "Enter Serial Number: ";
          while (serialNumber == "") {
                std::getline(_input, serialNumber);
            }
          _display << "Enter Location: ";
          while (location == "") {
                std::getline(_input, location);
            }
          Television tele{id, brand, model, purchasePrice, purchaseDate,
                      serialNumber, location };
          //            tele.setCustodian(custodian);
          am.storeAsset(std::make_shared<Television>(tele));
          break;
      }

      case Assets::HMD: {
          _display << "-------- HDM Information --------" << std::endl;

          _display << "Enter Serial Number: ";
          while (serialNumber == "") {
                std::getline(_input, serialNumber);
            }
          HMD hmd{id, brand, model, purchasePrice, purchaseDate,
              serialNumber };
          hmd.setCustodian(custodian);
          am.storeAsset(std::make_shared<HMD>(hmd));
          break;
      }
      }
} // MenuInterface::addAsset

void MenuInterface::disposeAsset()
{
    AssetRegister &ar = AssetRegister::instance();

    _display << "Enter Asset ID: ";
    std::string id;
    while (id == "") {
          std::getline(_input, id);
      }
    std::shared_ptr<Asset> asset = ar.retrieveAsset(id);
    if (asset == nullptr) {
          _display << id << " Does Not Exist" << std::endl << std::endl;
          return;
      }

    int day, month, year;
    Date disposalDate;
    std::string line;
    do {
          _display << "Enter Disposal Date [dd/mm/yyyy] or 't' for today: ";
          _input >> line;
          if (line[0] == 't') {
                disposalDate = Date::currentDate();
            } else {
                if (line.length() != 10) {
                      _display
                              << "Error: Invalid Date Format"
                              << std::endl;
                      continue;
                  }
                try {
                      day   = std::stoi(line.substr(0, 2));
                      month = std::stoi(line.substr(3, 4));
                      year  = std::stoi(line.substr(6, 9));
                  }
                catch (...) {
                    _display << "Error: Invalid Date Format"
                             << std::endl;
                    continue;
                }
                disposalDate =
                    Date{day,
                         static_cast<Date::Month>(month - 1),
                         year };
                if (!disposalDate.valid()) {
                      _display
                              << "Error: Invalid Date Format"
                              << std::endl;
                  }
                clearInputBuffer();
            }
      } while (!disposalDate.valid());

    char choice;
    do {
          _display << "Do you wish to dispose this asset? [y/n]: ";
          choice = getCharacterInput();
      } while (tolower(choice) != 'y' &&
               tolower(choice) != 'n');

    if (tolower(choice) == 'y') {
          asset.get()->dispose(disposalDate);
          _display << "Asset '" << asset.get()->id() << "' Disposed!" <<
                  std::endl <<
                  std::endl;
      } else {
          _display << "Disposal cancelled!" << std::endl;
      }
}

void MenuInterface::updateAsset()
{
    AssetRegister &ar = AssetRegister::instance();

    _display << "Enter Asset ID: ";
    std::string id;
    while (id == "") {
          std::getline(_input, id);
      }
    std::shared_ptr<Asset> asset = ar.retrieveAsset(id);
    if (asset == nullptr) {
          _display << id << " Does Not Exist" << std::endl << std::endl;
          return;
      }
    std::shared_ptr<Computer> c =
        std::dynamic_pointer_cast<Computer>(asset);
    std::shared_ptr<Phone> p =
        std::dynamic_pointer_cast<Phone>(asset);
    std::shared_ptr<Television> t =
        std::dynamic_pointer_cast<Television>(asset);
    std::shared_ptr<HMD> h =
        std::dynamic_pointer_cast<HMD>(asset);
    if (t != nullptr) {
          Television *television = t.get();
          _display << "Asset Found with ID "
                  "'" << id << "' to be a Television" << std::endl;
          _display << "Location: " << television->location() << std::endl <<
                  std::endl;
          _display << "Enter a new location: ";
          std::string location;

          while (location != "") {
                std::getline(_input, location);
            }

          char choice;
          do {
                _display << "Do you wish to save these changes? [y/n]:  ";
                choice = getCharacterInput();
            } while (tolower(choice) != 'y' &&
                     tolower(choice) != 'n');

          if (tolower(choice) == 'y') {
                television->setLocation(location);
                _display << "Changes saved!" << std::endl;
            } else {
                _display << "Changes reverted!" << std::endl;
            }
          return;
      } else if (c != nullptr) {
          _display << "Asset Found with ID "
                  "'" << id << "' to be a Computer" << std::endl;
      } else if (p != nullptr) {
          _display << "Asset Found with ID "
                  "'" << id << "' to be a Phone" << std::endl;
      } else if (h != nullptr) {
          _display << "Asset Found with ID "
                  "'" << id << "' to be a HMD" << std::endl;
      }

    _display << "Custodian: " << std::endl << asset.get()->custodian().get() <<
            std::endl;
    std::string custName, custDept, custPhone;
    Date employDate;

    _display << "Enter Custodian Name: ";
    while (custName == "") {
          std::getline(_input, custName);
      }
    _display << "Enter Custodian Department: ";
    while (custDept == "") {
          std::getline(_input, custDept);
      }
    _display << "Enter Phone Number: ";
    while (custPhone == "") {
          std::getline(_input, custPhone);
      }
    int day, month, year;
    std::string line;
    do {
          _display << "Enter Employment Date[dd/mm/yyyy]: ";
          _input >> line;
          if (line.length() != 10) {
                _display
                        << "Error: Invalid Date Format"
                        << std::endl;
                continue;
            }
          try {
                day   = std::stoi(line.substr(0, 2));
                month = std::stoi(line.substr(3, 4));
                year  = std::stoi(line.substr(6, 9));
            }
          catch (...) {
              _display << "Error: Invalid Date Format"
                       << std::endl;
              continue;
          }
          employDate =
              Date{day,
                   static_cast<Date::Month>(month - 1),
                   year };
          if (!employDate.valid()) {
                _display
                        << "Error: Invalid Date Format"
                        << std::endl;
            }
          clearInputBuffer();
      } while (!employDate.valid());
    char choice;
    do {
          _display << "Do you wish to save these changes? [y/n]:  ";
          choice = getCharacterInput();
      } while (tolower(choice) != 'y' &&
               tolower(choice) != 'n');

    if (tolower(choice) == 'y') {
          asset.get()->setCustodian(std::make_shared<Custodian>(
                            Custodian{custName,
                                      custDept,
                                      custPhone,
                                      employDate }));
          _display << "Changes saved!" << std::endl;
      } else {
          _display << "Changes reverted!" << std::endl;
      }
}

void MenuInterface::addMaintenance()
{
    AssetRegister &ar = AssetRegister::instance();

    _display << "Enter Asset ID: ";
    std::string id;
    while (id == "") {
          std::getline(_input, id);
      }
    std::shared_ptr<Asset> asset = ar.retrieveAsset(id);
    if (asset == nullptr) {
          _display << id << " Does Not Exist" << std::endl  << std::endl;
          return;
      }
    Date maintainDate;
    std::string name;
    int day, month, year;
    std::string line;

    do {
          _display << "Enter Maintenance Date[dd/mm/yyyy]: ";
          _input >> line;
          if (line.length() != 10) {
                _display << "Error: Invalid Date Format" << std::endl;
                continue;
            }
          try {
                day   = std::stoi(line.substr(0, 2));
                month = std::stoi(line.substr(3, 4));
                year  = std::stoi(line.substr(6, 9));
            }
          catch (...) {
              _display << "Error: Invalid Date Format" << std::endl;
              continue;
          }
          maintainDate = Date{day, static_cast<Date::Month>(month - 1), year };
          if (!maintainDate.valid()) {
                _display << "Error: Invalid Date Format" << std::endl;
            }
          clearInputBuffer();
      } while (!maintainDate.valid());
    _display << "Enter Maintainer Name: ";
    while (name == "") {
          std::getline(_input, name);
      }
    asset.get()->addMaintenance(Maintenance{maintainDate, name });
} // MenuInterface::addMaintenance

void MenuInterface::listAssetsByType()
{
    AssetRegister &ar = AssetRegister::instance();

    listByAssetSubMenu();
    std::vector<std::shared_ptr<Asset> > list;
    switch (char selection = getCharacterInput()) {
      case 'a':
          _display << "All asset type selected" << std::endl;
          list = ar.retrieveAssetByType("ALL");
          break;
      case 'c':
          _display << "Computer asset type selected" << std::endl;
          list = ar.retrieveAssetByType("COMPUTER");
          break;

      case 'p':
          _display << "Phone asset type selected" << std::endl;
          list = ar.retrieveAssetByType("PHONE");
          break;

      case 't':
          _display << "Television type asset selected" << std::endl;
          list = ar.retrieveAssetByType("TELEVISION");
          break;

      case 'h':
          _display << "HMD asset type selected" << std::endl;
          list = ar.retrieveAssetByType("HMD");
          break;

      case 'b':
          return;

          break;

      default:
          _display << "Sorry, \'" << selection
                   << "\' is not a valid option, please try again."
                   << std::endl;
          return;

          break;
      }
    if (list.size() == 0) {
          _display << "No Assets of this type on record" << std::endl <<
        std::endl;
          return;
      }
    _display << std::setw(15) << "ID" << std::setw(15) << "Type" <<
            std::endl;
    for (std::shared_ptr<Asset> asset : list) {
          if (std::dynamic_pointer_cast<Computer>(asset)) {
                _display << std::setw(15) << asset.get()->id() <<
                        std::setw(15) << "Computer"
                         << std::endl;
            }
          if (std::dynamic_pointer_cast<Phone>(asset)) {
                _display << std::setw(15) << asset.get()->id() <<
                        std::setw(15) << "Phone"
                         << std::endl;
            }
          if (std::dynamic_pointer_cast<Television>(asset)) {
                _display << std::setw(15) << asset.get()->id() <<
                        std::setw(15) << "Television"
                         << std::endl;
            }
          if (std::dynamic_pointer_cast<HMD>(asset)) {
                _display << std::setw(15) << asset.get()->id() <<
                        std::setw(15) << "HMD"
                         << std::endl;
            }
      }

    _display << std::endl << "Enter any key to return to main menu" << std::endl;
    getCharacterInput();

    _display << std::endl;
} // MenuInterface::listAssetsByType

void MenuInterface::listAssetsByCustodian()
{
    AssetRegister &ar = AssetRegister::instance();

    std::string name;

    _display << "Enter Custodian Name: ";
    while (name == "") {
          std::getline(_input, name);
      }
    std::vector<std::shared_ptr<Asset> > list =
        ar.retrieveAssetByCustodian(name);
    if (list.size() == 0) {
          _display << "Custodain not found on any assets" << std::endl  <<
        std::endl;
          return;
      }
    _display << std::setw(15) << "ID" << std::setw(15) << "Type" <<
            std::endl;
    for (std::shared_ptr<Asset> asset : list) {
          if (std::dynamic_pointer_cast<Computer>(asset)) {
                _display << std::setw(15) << asset.get()->id() <<
                        std::setw(15) << "Computer"
                         << std::endl;
            }
          if (std::dynamic_pointer_cast<Phone>(asset)) {
                _display << std::setw(15) << asset.get()->id() <<
                        std::setw(15) << "Phone"
                         << std::endl;
            }
          if (std::dynamic_pointer_cast<Television>(asset)) {
                _display << std::setw(15) << asset.get()->id() <<
                        std::setw(15) << "Television"
                         << std::endl;
            }
          if (std::dynamic_pointer_cast<HMD>(asset)) {
                _display << std::setw(15) << asset.get()->id() <<
                        std::setw(15) << "HMD"
                         << std::endl;
            }
      }
    _display << std::endl << "Enter any key to return to main menu" << std::endl;
    getCharacterInput();


    _display << std::endl;
}

void MenuInterface::findAsset()
{
    AssetRegister &ar = AssetRegister::instance();

    std::shared_ptr<Asset> asset;

    findAssetSubMenu();
    switch (char selection = getCharacterInput()) {
      case 'a': {
          _display << "Enter Asset ID: ";
          std::string id;
          while (id == "") {
                std::getline(_input, id);
            }
          asset = ar.retrieveAsset(id);
          if (asset == nullptr) {
                _display << id << " Does Not Exist" << std::endl  <<
                    std::endl;
                return;
            }
          break;
      }

      case 's': {
          _display << "Enter Asset Serial Number: ";
          std::string serialNumber;
          while (serialNumber == "") {
                std::getline(_input, serialNumber);
            }
          asset = ar.retrieveAssetBySerial(serialNumber);
          if (asset == nullptr) {
                _display << serialNumber << " Does Not Exist" << std::endl <<
                    std::endl;
                return;
            }
          break;
      }
      case 'b':
          return;

          break;

      default:
          _display << "Sorry, \'" << selection
                   << "\' is not a valid option, returning to main menu."
                   << std::endl;
          return;

          break;
      }

    bool end = false;
    do {
          _display << asset << std::endl;
          _display << std::endl;
          _display << "(c)ustodian details" << std::endl;
          _display << "(m)aintenance history" << std::endl;
          _display << "(b)ack to main menu" << std::endl;
          switch (char selection = getCharacterInput()) {
            case 'c': {
                _display << "Custodian for asset: " << asset.get()->id()
                         << std::endl;
                std::shared_ptr<Custodian> custodian =
                    asset.get()->custodian();
                if (custodian == nullptr) {
                      _display << "No Custodian Available" << std::endl;
                  } else {
                      Custodian *cust = custodian.get();
                      _display << cust << std::endl;
                  }
                break;
            }

            case 'm':
                _display << "Maintenance for asset: " << asset.get()->id()
                         << std::endl;
                if (asset.get()->maintenance().size() == 0) {
                      _display << "No Maintenance History" << std::endl;
                  } else {
                      for (Maintenance main :
                           asset.get()->maintenance()) {
                        _display << " " << main;
                    }
                  }
                break;

            case 'b':
                return;

                break;

            default:
                _display << "Sorry, \'" << selection
                         <<
                        "\' is not a valid option. Taking you back to asset details."
                         << std::endl;
                continue;
                break;
            }
          _display << std::endl << "(r)eturn to asset details" << std::endl;
          _display << "(b)ack to main menu" << std::endl;
          switch (char selection = getCharacterInput()) {
            case 'r': {
                continue;
                break;
            }

            case 'b':
                return;

                break;

            default:
                _display << "Sorry, \'" << selection
                         <<
                        "\' is not a valid option. Taking you back to asset details."
                         << std::endl;
                break;
            }
      } while (!end);
    _display << std::endl;
} // MenuInterface::findAsset

void MenuInterface::clearInputBuffer()
{
    _input.clear();
    _input.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}
