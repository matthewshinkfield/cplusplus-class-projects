#ifndef COMPUTER_H
#define COMPUTER_H
#include "asset.h"

/**
 * @brief The Computer class represents a Computer in the asset register
 */
class Computer : public Asset
{
public:
    /**
     * @brief Computer Constructor for Computer
     * @param id
     * @param brand
     * @param model
     * @param purchasePrice
     * @param purchaseDate
     * @param serialNumber
     * @param operatingSystem
     * @param networkIdentifier
     * @param laptop
     */
    Computer(const std::string &id, const std::string &brand,
             const std::string &model, double purchasePrice, const Date &purchaseDate,
             const std::string &serialNumber, const std::string &operatingSystem,
             const std::string &networkIdentifier, const bool laptop);

    /**
     * @brief Computer Constructor for Testing Class
     * @param id
     * @param brand
     * @param model
     * @param purchasePrice
     * @param purchaseDate
     * @param serialNumber
     * @param operatingSystem
     * @param laptop
     */
    Computer(const std::string &id, const std::string &brand,
             const std::string &model, double purchasePrice, const Date &purchaseDate,
             const std::string &serialNumber, const std::string &operatingSystem,
             bool laptop);

    /**
     * @brief serialNumber retrieve serial number
     * @return _serialNumber serial number of Computer
     */
    std::string serialNumber() const;
    /**
     * @brief setSerialNumber set serial number
     * @param serialNumber serial number for Computer
     */
    void setSerialNumber(std::string &serialNumber);

    /**
     * @brief operatingSystem retrieve operating system
     * @return operatingSystem operating system of Computer
     */
    const std::string &operatingSystem() const;
    /**
     * @brief setOperatingSystem set operating system
     * @param operatingSystem operating system for Computer
     */
    void setOperatingSystem(std::string &operatingSystem);

    /**
     * @brief networkIdentifier retrieve network identifier
     * @return _networkIdentifier network identifier of Computer
     */
    const std::string &networkIdentifier() const;
    /**
     * @brief setNetworkIdentifier set network identifier
     * @param networkIdentifier network identifier for Computer
     */
    void setNetworkIdentifier(std::string &networkIdentifier);

    /** @brief custodian retrieve custodian
     * @return _custodian of Computer, nullptr if does not exist
     */
    const std::shared_ptr<Custodian> custodian() const;
    /**
     * @brief setCustodian Set custodian
     * @param custodian Custodian for Computer
     */
    void setCustodian(std::shared_ptr<Custodian> cust);
    /**
     * @brief print print data from Computer
     * @param strm ostream for easy string merging
     */
    void print(std::ostream &strm) const;
private:
    std::string _serialNumber; /**< serial number of Computer */
    std::string _operatingSystem;/**< operating system of Computer */
    std::string _networkIdentifier;/**< network identifir of Computer */
    std::shared_ptr<Custodian> _custodian; /**< custodian of Computer */
};

#endif // COMPUTER_H
