#include "asset.h"
#include <math.h>
#include <iomanip>

const std::string &Asset::id() const
{
    return _id;
}

void Asset::setId(std::string &id)
{
    _id = id;
}

const std::string &Asset::brand() const
{
    return _brand;
}

void Asset::setBrand(std::string &brand)
{
    _brand = brand;
}

const std::string &Asset::model() const
{
    return _model;
}

void Asset::setModel(std::string &model)
{
    _model = model;
}

double Asset::purchasePrice() const
{
    return _purchasePrice;
}

void Asset::setPurchasePrice(double purchasePrice)
{
    _purchasePrice = purchasePrice;
}

const Date &Asset::purchaseDate() const
{
    return _purchaseDate;
}

void Asset::setPurchaseDate(Date &purchaseDate)
{
    _purchaseDate = purchaseDate;
}

const Date &Asset::disposalDate() const
{
    return _disposalDate;
}

double Asset::lifeSpan() const
{
    return _lifeSpan;
}

void Asset::setLifeSpan(double lifeSpan)
{
    _lifeSpan = lifeSpan;
}

const std::vector<Maintenance> Asset::maintenance() const
{
    return _maintenance;
}

void Asset::addMaintenance(Maintenance record)
{
    _maintenance.push_back(record);
}

void Asset::dispose(Date &disposalDate)
{
    _disposalDate = disposalDate;
}

std::ostream &operator <<(std::ostream &strm, const Asset *a)
{
    std::string maintenance = "Never";
    if (a->maintenance().size() > 0) {
          Maintenance m = a->maintenance().back();
          maintenance = m.maintenanceDate().format() + " by "
                        + m.maintainer();
      }
    std::shared_ptr<Custodian> cust = nullptr;
    if (a->custodian()) {
          cust = a->custodian();
      }
    strm.precision(2);
    // Default Lines to Print
    // Asset ID
    strm << std::left << std::setw(20) << "Asset ID:" << a->id() << std::endl;

    // Asset Brand
    strm << std::setw(20) << "Brand:" << a->brand() << std::endl;

    // Asset Model
    strm << std::setw(20) << "Model:" << a->model() << std::endl;

    // Asset Purchase Price
    strm << std::setw(20) << "Purchase Price:" << std::fixed << "$"
         << (a->purchasePrice())
         << std::endl;

    // asset Purchase Date
    strm << std::setw(20) << "Purchase Date:" << a->purchaseDate().format() <<
            std::endl;

    // Asset disposal date
    if (a->disposalDate().valid()) {
          strm << std::setw(20) << "Disposal Date:"
               << a->disposalDate()
               << std::endl;
          // Asset  Depreciated Value
          strm << std::setw(20) << "Depreciated Value:" << std::fixed
               << "$" << (a->purchasePrice()
                     - a->calculateDepreciation(a->disposalDate()))
               << " (at disposal)" << std::endl;
      } else {
          // Asset Depreciated Value
          strm << std::setw(20) << "Depreciated Value:"
               << "$" << (a->purchasePrice() -
                     a->calculateDepreciation(Date::currentDate()))
               << " (deprecation of $" << a->calculateDepreciation(
              Date::currentDate()) << " at " << Date::currentDate() << ")"
               << std::endl;
          // Print Line from Derived Class
          a->print(strm);
          strm << std::endl;
          // Additional Lines to Print
          if (cust == nullptr) {
                strm << std::setw(20) << "Custodian:" << "Not found"
                     << std::endl;
            } else {
                strm << std::setw(20) << "Custodian:" << cust.get()->name()
                     << std::endl;
            }
      }

    strm << std::setw(20) << "Last Maintenance:" << maintenance;

    return strm;
} // <<

double Asset::calculateDepreciation(const Date &date) const
{
    double difference  = _purchaseDate.differenceInDays(date);
    double deprecation =
        _purchasePrice * (difference / 365.0)
        * (1.0 / double (_lifeSpan));

    return double ((deprecation * 100) / 100);
}

Asset::Asset(const std::string &id, const std::string &brand, const std::string &model,
             const double purchasePrice, const Date &purchaseDate) : _id{id},
    _brand{brand}, _model{model},
    _purchasePrice{purchasePrice},
    _purchaseDate{purchaseDate}
{
}
