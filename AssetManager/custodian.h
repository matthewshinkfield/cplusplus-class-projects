#ifndef CUSTODIAN_H
#define CUSTODIAN_H
#include "date.h"

/**
 *  @brief Custodian class for Asset types
 */
class Custodian
{
public:
    /**
     * @brief ~Custodian Custodian Deconstructor
     */
    virtual ~Custodian() {
    }

    /**
     * @brief Custodian Blank Custodian Constructor
     */
    Custodian() {
    }

    /**
     * @brief Custodian Constructor for Custodian
     * @param name
     * @param department
     * @param phoneNumber
     * @param employmentStart
     */
    Custodian(const std::string &name, const std::string &department,
              const std::string &phoneNumber, Date &employmentStart);

    /**
     * @brief name retrieve name
     * @return _name name of Custodian
     */
    const std::string &name() const;

    /**
     * @brief setName set name
     * @param name name for Custodian
     */
    void setName(std::string &name);

    /**
     * @brief department retrieve department
     * @return _department department of Custodian
     */
    const std::string &department() const;

    /**
     * @brief setDepartment set department
     * @param department department for Custodian
     */
    void setDepartment(std::string &department);

    /**
     * @brief phoneNumber retrieve phone number
     * @return _phoneNumber phone number of Custodian
     */
    const std::string &phoneNumber() const;

    /**
     * @brief setPhoneNumber set phone number
     * @param phoneNumber phone number for Custodian
     */
    void setPhoneNumber(std::string &phoneNumber);

    /**
     * @brief employmentStart retrieve employ date
     * @return _employmentStart employ date of Custodian
     */
    const Date &employmentStart() const;

    /**
     * @brief setEmploymentStart set employment start
     * @param employmentStart employ date for Custodian
     */
    void setEmploymentStart(Date &employmentStart);
private:
    friend std ::ostream &operator <<(std::ostream &strm, const Custodian *c);
    std::string _name; /**< name of Custodian */
    std::string _department; /**< department of Custodian */
    std::string _phoneNumber; /**< phone number of Custodian */
    Date _employmentStart; /**< employment start of Custodian */
};

#endif // CUSTODIAN_H
