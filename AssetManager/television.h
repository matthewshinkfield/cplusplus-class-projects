#ifndef TELEVISION_H
#define TELEVISION_H
#include "asset.h"

/**
    @brief Television in the asset register
 */
class Television : public Asset
{
public:
    /**
     * @brief Television Television Constructor
     * @param id
     * @param brand
     * @param model
     * @param purchasePrice
     * @param purchaseDate
     * @param serialNumber
     * @param location
     */
    Television(const std::string &id, const std::string &brand,
               const std::string &model, double purchasePrice,
               const Date &purchaseDate, const std::string &serialNumber,
               const std::string &location);

    /**
     * @brief serialNumber retrieve serial number
     * @return _serialNumber serial number of Television
     */
    std::string serialNumber() const;
    /**
     * @brief setSerialNumber set serial number
     * @param serialNumber serial number for Television
     */
    void setSerialNumber(std::string &serialNumber);
    /**
     * @brief location retrieve location
     * @return _location location of Television
     */
    const std::string &location() const;
    /**
     * @brief setLocation set location
     * @param location location of television
     */
    void setLocation(std::string &location);
    /**
     * @brief print print data from Television
     * @param strm ostream for easy string merging
     */
    void print(std::ostream &strm) const;
private:
    std::string _serialNumber; /**< serial number for Television */
    std::string _location; /**< location for Television */
};

#endif // TELEVISION_H
