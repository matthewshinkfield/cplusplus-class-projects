#ifndef DATE_H
#define DATE_H
#include <string>

/**
 * @brief The Date class represents a calendar date, in Australian date format (day/month/year).
 */
class Date {
public:
    enum Month { January = 0, February, March, April, May, June, July, August,
             September, October,
             November, December };
    /**
     * @brief Date construct a blank (invalid) date object.
     */
    Date();

    /**
     * @brief Date construct a date with the given details. Note: no validation takes place.
     * @param day the day of the month.
     * @param month the month.
     * @param year the full year e.g. 2016
     */
    Date(int day, Month month, int year);

    /**
     * @brief currentDate get the current date
     * @return a Date containing the current date
     */
    static Date currentDate();

    /**
     * @brief format create a string representation of the date.
     * @return the string representing this date.
     */
    std::string format() const;

    /**
     * @brief valid tests to see if this date has a valid value.
     * @return true if the date is valid.
     */
    bool valid() const;

    /**
     * @brief differenceInDays calculate the number between this date and another date.
     * @param other the date to calculate to or from.
     * @return the absolute number of days between this date and other. -1 on error.
     */
    int differenceInDays(const Date &other) const;

    /**
     * @brief operator tm convert to a struct tm.
     */
    operator tm() const;

    /**
     * @brief operator == compares two date objects for equality based on date.
     * @param other the date to compare to
     * @return true if the objects represent the same date.
     */
    bool operator ==(const Date &other) const;

    /**
     * @brief operator != the inequality operator, logically oposite of equality test.
     * @param other the date to compare to
     * @return true if the objects represent different dates.
     */
    bool operator !=(const Date &other) const;
private:
    int _year; /**< the year e.g. 2017 */
    Month _month; /** the month */
    int _day; /**< the day of month e.g. 31 */
};

/**
 * @brief operator << overloads the stream operator for use with a date object.
 * @param os the output stream.
 * @param date the date object
 *
 */
std::ostream &operator <<(std::ostream &os, const Date &date);

#endif // DATE_H
