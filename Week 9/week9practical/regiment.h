#ifndef REGIMENT_H
#define REGIMENT_H
#include "unit.h"
#include <ostream>
#include <vector>

/**
 * @brief The Regiment class represents the front-line units engaged in a battle.
 */
class Regiment {
public:
    /**
     * @brief generateRandom creates a Regiment with a random set of units.
     */
    static Regiment generateRandom(int production);

    /**
     * @brief isDefeated
     * @return
     */
    bool isDefeated() const;

    /**
     * @brief attack launches an offensive attack against the oposing Regiment, causing damage to
     * their units.
     * @param defending the enemy to attack. This is the defending Regiment for this
     */
    void attack(Regiment &defending) const;
private:
    Regiment();
    std::vector<Unit *> _units;

    static Unit *randomUnit();

    friend std ::ostream &operator <<(std::ostream &os, const Regiment &regiment);
};

#endif // REGIMENT_H
