// Author: Matthew Shinkfield
// Generated: 27 Sep 2017 @ 23:24:53
#include "chariot.h"

Chariot::Chariot(int initialHp, int productionCost, int attack) : Unit{initialHp},
    _productionCost{productionCost}, _attack{attack}
{
}

int Chariot::attack()
{
    return _attack * randomAttack();
}

int Chariot::productionCost()
{
    return _productionCost;
}
