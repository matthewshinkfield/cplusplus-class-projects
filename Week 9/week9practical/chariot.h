#ifndef CHARIOT_H
#define CHARIOT_H
#include "unit.h"

/**
 *  @author Matthew Shinkfield
 *  @file chariot.h
 *  @date 27 Sep 2017
 */
class Chariot : public Unit
{
public:
    Chariot(int initialHp = 30, int productionCost = 60, int attack = 3);
    int attack();
    int productionCost();
private:
    int _productionCost;
    int _attack;
};

#endif // CHARIOT_H
