// Author: Matthew Shinkfield
// Generated: 27 Sep 2017 @ 23:23:35
#include "archer.h"

Archer::Archer(int initialHp, int productionCost, int attack) : Unit{initialHp},
    _productionCost{productionCost}, _attack{attack}
{
}

int Archer::attack()
{
    return _attack * randomAttack();
}

int Archer::productionCost()
{
    return _productionCost;
}

bool Archer::skipAttack() const
{
    return _skipAttack;
}

void Archer::setSkipAttack(bool skipAttack)
{
    _skipAttack = skipAttack;
}
