#include "regiment.h"
#include "unittypes.h"
#include <ctime>
#include <random>
#include <iomanip>
#include <algorithm>
#include <iostream>

Regiment::Regiment() {
}

Regiment Regiment::generateRandom(int production) {
    Regiment reg;
    int productionCost;
    do {
          Unit *unit = reg.randomUnit();
          productionCost = unit->productionCost();
          if (productionCost <= production) {
                reg._units.push_back(unit);
                production -= unit->productionCost();
            }
      } while (production > productionCost);
    return reg;
}

bool Regiment::isDefeated() const {
    int alive = 0;
    for (Unit *unit : _units) {
          if (!unit->isDead()) {
                alive++;
            }
      }
    if (alive > 1)
        return false;
    return true;
}


void Regiment::attack(Regiment &defending) const {
    //TODO : Add checks for archer and skip their every other turn
    //TODO : Also i believe you have to skip back around to the next alive unit
    Unit *defUnit = nullptr;
    int defPos = 0; /** @brief: */
    int defSize = defending._units.size();
    for (Unit *unit : _units) {
          if (unit->isDead()) {
                continue;
            }
          if (Archer *archer = dynamic_cast<Archer *>(unit)) {
                if (archer->skipAttack() == true) {
                      archer->setSkipAttack(false);
                      continue;
                  }
                archer->setSkipAttack(true);
            }
          if (defPos >= defSize) {
                defPos = 0;
            }
          auto it = std::find_if(defending._units.begin() + defPos,
                                 defending._units.end(),
                                 [](const Unit *obj) {
            return obj->isDead() == false;
        });
          if (it != defending._units.end()) {
                int index = std::distance(
                    defending._units.begin(), it);
                defPos = index + 1;
                defUnit = defending._units[index];
                defUnit->takeDamage(unit->attack());
                if (defUnit->hp() <= 0) {
                      defUnit->setDead(true);
                  }
            }
      }
}

Unit *Regiment::randomUnit() {
    // This has been provided for your convenience.
    static std::mt19937 randomEngine{static_cast<uint32_t>(time(NULL)) };
    static std::uniform_int_distribution<int> type(0, 2);
    int unitType = type(randomEngine);
    switch (unitType) {
      case 0:
          return new Soldier();
      case 1:
          return new Archer();
      case 2:
          return new Chariot();
      }
    // This should never happen.
    return nullptr;
}

std::ostream &operator <<(std::ostream &os, const Regiment &regiment) {
    int archers = 0;
    int chariots = 0;
    int soldiers = 0;
    int hp = 0;
    for (Unit *unit : regiment._units) {
          if (unit->hp() > 0) {
                if (dynamic_cast<Archer *>(unit)) {
                      archers++;
                  } else if (dynamic_cast<Chariot *>(unit)) {
                      chariots++;
                  } else if (dynamic_cast<Soldier *>(unit)) {
                      soldiers++;
                  }
                hp += unit->hp();
            }
      }
    os << std::left << std::setw(15) << "Soldiers:" << std::setw(5) << soldiers
       << std::setw(15) << "Archers:" << std::setw(5) << archers
       << std::setw(15) << "Chariots:" << std::setw(5) << chariots << std::endl
       << "Total Remaining HP: " << hp << std::endl;
    return os;
}
