#ifndef SOLDIER_H
#define SOLDIER_H
#include "unit.h"

/**
 *  @author Matthew Shinkfield
 *  @file soldier.h
 *  @date 27 Sep 2017
 */
class Soldier : public Unit
{
public:
    Soldier(int initialHp = 20, int productionCost = 20, int attack = 1);
    int attack();
    int productionCost();
private:
    int _productionCost;
    int _attack;
};

#endif // SOLDIER_H
