// Author: Matthew Shinkfield
// Generated: 27 Sep 2017 @ 23:23:21
#include "soldier.h"

Soldier::Soldier(int initialHp, int productionCost, int attack) : Unit{initialHp},
    _productionCost{productionCost}, _attack{attack}
{
}

int Soldier::attack()
{
    return _attack * randomAttack();
}

int Soldier::productionCost()
{
    return _productionCost;
}
