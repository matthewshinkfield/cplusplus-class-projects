#ifndef ARCHER_H
#define ARCHER_H
#include "unit.h"

/**
 *  @author Matthew Shinkfield
 *  @file archer.h
 *  @date 27 Sep 2017
 */
class Archer : public Unit
{
public:
    /**
     * @brief Archer Constructor for Class
     * @param initialHp
     * @param productionCost
     * @param attack
     */
    Archer(int initialHp = 20, int productionCost = 20, int attack = 2);
    int attack();
    int productionCost();
    /**
     * @brief skipAttack Check if Archer needs to skip next attack
     * @return true if needs to skip, otherwise
     */
    bool skipAttack() const;
    /**
     * @brief setSkipAttack Set if user needs to skip next attack.
     * @param skipAttack true or false
     */
    void setSkipAttack(bool skipAttack);
private:
    bool _skipAttack = false;
    int _productionCost;
    int _attack;
};

#endif // ARCHER_H
