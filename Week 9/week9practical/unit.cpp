#include "unit.h"
#include <random>
#include <ctime>

Unit::Unit(int initialHp) : _hp{initialHp} {
}

Unit::~Unit() {
}

void Unit::takeDamage(int damage) {
    _hp = _hp - damage;
}

int Unit::hp() const {
    return _hp;
}

bool Unit::isDead() const
{
    return _dead;
}

void Unit::setDead(bool dead)
{
    _dead = dead;
}

int Unit::randomAttack() const
{
    // Don't worry, this function is correct!
    static std::mt19937 randomEngine{static_cast<uint32_t>(time(NULL)) };
    static std::uniform_int_distribution<int> attackVal(2, 12);
    return attackVal(randomEngine);
}
