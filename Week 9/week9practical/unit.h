#ifndef UNIT_H
#define UNIT_H

/**
 * @brief The Unit class represents a single unit e.g. a Soldier or Archer.
 */
class Unit {
public:
    /**
     * @brief Unit the unit constructor
     * @param initialHp the HP to set when this unit is created.
     */
    Unit(int initialHp);
    virtual ~Unit();

    /**
     * @brief attack calculate the damage this unit inflicts this round. A dead unit can not inflict
     * damage.
     * @return the damage inflected.
     */
    virtual int attack() = 0;

    /**
     * @brief takeDamage reduces this unit's HP by the give value
     * @param damage the damage to inflict on this unit
     */
    void takeDamage(int damage);

    /**
     * @brief hp retreive this unit's current HP
     * @return the current HP
     */
    int hp() const;

    /**
     * @brief isDead retrieve this units dead state
     * @return the current death state
     */
    bool isDead() const;

    /**
     * @brief setDead set the units dad state
     * @param dead true of false
     */
    void setDead(bool dead);

    /**
     * @brief productionCost the cost to produce this unit.
     * @return the production cost
     */
    virtual int productionCost() = 0;
protected:
    /**
     * @brief randomAttack returns a random number result of two die rolls to determine attack
     * strength.
     * @return
     */
    int randomAttack() const;
private:
    bool _dead = false;
    int _hp; /**< this unit's current HP */
};

#endif // UNIT_H
