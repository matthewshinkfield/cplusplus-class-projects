TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    unit.cpp \
    regiment.cpp \
    soldier.cpp \
    archer.cpp \
    chariot.cpp

HEADERS += \
    unit.h \
    unittypes.h \
    regiment.h \
    soldier.h \
    archer.h \
    chariot.h
