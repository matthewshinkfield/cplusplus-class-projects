#include "document.h"

Document::Document(const QString &filename, const QString &text): _filename{filename}, _text{text}
{

}

const QString &Document::filename() const
{
    return _filename;
}

const QString &Document::text() const
{
    return _text;
}

void Document::setText(const QString &text)
{
    _text = text;
}
