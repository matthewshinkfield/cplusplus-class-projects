#include "editorwindow.h"
#include <QMessageBox>
#include <QtWidgets>
#include "ui_editorwindow.h"

EditorWindow::EditorWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::EditorWindow) {
  ui->setupUi(this);

  ui->statusBar->showMessage("Text Editor by Matthew Shinkfield", 5000);
  document = new Document("untitled", "");
  setWindowTitle("untitled - EditorWindow");
}

EditorWindow::~EditorWindow() {
  delete ui;
}

void EditorWindow::on_actionNew_triggered() {
  if (windowTitle().startsWith('*')) {
    QMessageBox::StandardButton reply = QMessageBox::question(
        this, "File Modified", "This file has been modified, do you wish to save changes?",
        QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);

    if (reply == QMessageBox::Yes) {
      on_actionSave_triggered();
      if (windowTitle().startsWith('*')) {
        return;
      }
    } else if (reply == QMessageBox::Cancel) {
      return;
    }
  }

  document = new Document("untitled", "");
  ui->plainTextEdit->document()->setPlainText("");
  setWindowTitle("untitled - EditorWindow");
  ui->statusBar->showMessage("Created New Document", 5000);
}

void EditorWindow::on_actionOpen_triggered() {
  if (windowTitle().startsWith('*')) {
    QMessageBox::StandardButton reply = QMessageBox::question(
        this, "File Modified", "This file has been modified, do you wish to save changes?",
        QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);

    if (reply == QMessageBox::Yes) {
      on_actionSave_triggered();
      if (windowTitle().startsWith('*')) {
        return;
      }
    } else if (reply == QMessageBox::Cancel) {
      return;
    }
  }

  QString path =
      QFileDialog::getOpenFileName(this, tr("Open File"), nullptr, tr("Text File (*.txt)"));

  QFile file(path);
  if (!file.open(QIODevice::ReadOnly)) {
    QMessageBox::information(0, "error", file.errorString());
  }
  QTextStream in(&file);
  QString text = in.readAll();
  document = new Document(path, text);
  ui->plainTextEdit->document()->setPlainText(text);
  QFileInfo fileinfo(file);
  setWindowTitle(fileinfo.fileName() + " - EditorWindow");
  ui->statusBar->showMessage("Loaded " + document->filename(), 5000);
  file.close();
}

void EditorWindow::on_actionSave_triggered() {
  QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), document->filename(),
                                                  tr("Text File (*.txt)"));
  if (document->filename() != fileName) {
    document = new Document{fileName, ui->plainTextEdit->document()->toPlainText()};
  } else {
    document->setText(ui->plainTextEdit->document()->toPlainText());
  }

  QFile file(fileName);
  if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
    QMessageBox::information(0, "error", file.errorString());
    return;
  }

  QTextStream out(&file);
  out << document->text();

  QFileInfo fileinfo(file);
  setWindowTitle(fileinfo.fileName() + " - EditorWindow");
  ui->statusBar->showMessage("Saved " + document->filename(), 5000);
  file.close();
}

void EditorWindow::on_plainTextEdit_textChanged() {
  if (!windowTitle().contains('*')) {
    setWindowTitle(windowTitle().prepend("*"));
  }
}

void EditorWindow::on_actionExit_triggered() {
  if (windowTitle().startsWith('*')) {
    QMessageBox::StandardButton reply = QMessageBox::question(
        this, "File Modified", "This file has been modified, do you wish to save changes?",
        QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);

    if (reply == QMessageBox::Yes) {
      on_actionSave_triggered();
      if (windowTitle().startsWith('*')) {
        return;
      }
    } else if (reply == QMessageBox::Cancel) {
      return;
    }
  }
  QApplication::quit();
}
