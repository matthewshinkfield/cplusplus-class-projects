#ifndef EDITORWINDOW_H
#define EDITORWINDOW_H

#include <QMainWindow>
#include "document.h"

namespace Ui {
class EditorWindow;
}

class EditorWindow : public QMainWindow {
  Q_OBJECT

 public:
  explicit EditorWindow(QWidget *parent = 0);
  ~EditorWindow();
 public slots:
  void on_actionNew_triggered();
  void on_actionOpen_triggered();
  void on_actionSave_triggered();
  void on_actionExit_triggered();
  void on_plainTextEdit_textChanged();

 private:
  Ui::EditorWindow *ui;
  Document *document;
};

#endif  // EDITORWINDOW_H
