#ifndef DOCUMENT_H
#define DOCUMENT_H
#include <QString>

class Document
{
public:
    Document(const QString &filename, const QString &text);

    const QString &filename() const;
    const QString &text() const;
    void setText(const QString &text);
private:
    QString _filename;
    QString _text;
};

#endif // DOCUMENT_H
